#!/usr/bin/env bash
set -ev

mkdir temp1
cd temp1
darcs init
yes | head -n 4000 | xargs echo -n > foo
darcs add foo
darcs record -a -m 'foo' foo
yes | head -n 300000 | xargs echo -n > foo
darcs what
cd ..
