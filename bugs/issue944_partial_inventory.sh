#!/usr/bin/env bash

set -ev

test $DARCS || DARCS=$PWD/../darcs
ACTUAL_DARCS=`which $DARCS`
DARCSPATH=`dirname $ACTUAL_DARCS`
PATH="$DARCSPATH:$PATH"
export PATH

# create base repository
rm -rf temp1
mkdir temp1
cd temp1
darcs init

echo first > a
darcs add a
darcs record -am first
darcs tag --checkpoint 'Tag 1'

echo second > b
darcs add b
darcs record -am second
darcs tag --checkpoint 'Tag 2'

echo third > c
darcs add c
darcs record -am third
darcs tag --checkpoint 'Tag 3'

# create a partial copy of the base repository and modify it
cd ..
rm -rf temp2
darcs get --partial temp1 temp2
cd temp2

# instead of the following three commands one could also use darcs optimize
echo mistake > a
darcs record -am mistake
darcs unrecord -ap mistake

# now check the repository
darcs check
# => darcs: failed to read patch: ...

