#!/usr/bin/env bash
set -ev

# add should fail on files it can't read (because it would fail to record it later anyway)

if echo $OS | grep -i windows; then
  echo This test does not work on Windows
  exit 0
fi

rm -rf temp1
mkdir temp1
cd temp1
darcs init
touch no_perms.txt
chmod 0000 no_perms.txt
# surely there is an easier way to write this 'not'
# i'm think of the 'finally' in try/catch/finally
dacrs add no_perms.txt 2> log && (chmod 0755 no_perms.txt ; exit 1) || chmod 0755 no_perms.txt 
grep -i 'permission denied' log

cd ..
rm -rf temp1
