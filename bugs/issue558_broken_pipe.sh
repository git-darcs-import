#!/usr/bin/env bash

# For issue588, "amend-record --look-for-adds end up with two "addfile" entries"

set -ev
rm -rf temp1
darcs init --repodir temp1

export EMAIL=me

cd temp1

date > f
darcs add f
darcs rec -am p1
for (( i=0 ; i < 500; i=i+1 )); do
  echo $i >> f;
  darcs rec -am p$i
done

darcs changes 2> err | head

touch correcterr

diff correcterr err

cd ..

rm -rf temp1
