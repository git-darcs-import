<?xml version="1.0" encoding="utf-8"?>

<!--
 template for converting a darcs current/ directory listing from XML to
 XHTML.  This template expects the following external variables:

   cgi-program    - path to the CGI executable, to place in links
   sort-by        - field to sort by
   stylesheet     - path to the CSS stylesheet

 the input XML must have the following structure, aside from what common.xslt
 expects:

 <files>
   <directory name="" modified=""/>
   <file name="" modified=""/>
 </files>
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:include href="common.xslt"/>

  <xsl:template match="path">
    <h1 class="target">
      files in <span class="path"><a href="{$cgi-program}">projects</a></span> / <span class="path"><xsl:apply-templates/></span>
    </h1>
  </xsl:template>
  
  <xsl:template match="files">
    <table>
      <tr class="browse">
        <th>
          <a class="sort" href="{$command}?c=browse&amp;s=name">name</a>
          <a class="revsort" href="{$command}?c=browse&amp;s=revname"> (rev)</a>
        </th>
        <th>
          <a class="sort" href="{$command}?c=browse&amp;s=ts">modified</a>
          <a class="revsort" href="{$command}?c=browse&amp;s=revts"> (rev)</a>
        </th>
        <th></th>
        <th></th>
      </tr>
      
      <xsl:choose>
        <xsl:when test="$sort-by = 'ts'">
          <xsl:apply-templates>
            <xsl:sort select="name(self::node())"/>
            <xsl:sort select="@ts"/>
          </xsl:apply-templates>
        </xsl:when>
        <xsl:when test="$sort-by = 'revts'">
          <xsl:apply-templates>
            <xsl:sort select="name(self::node())"/>
            <xsl:sort select="@ts" order="descending"/>
          </xsl:apply-templates>
        </xsl:when>
        <xsl:when test="$sort-by = 'name'">
          <xsl:apply-templates>
            <xsl:sort select="name(self::node())"/>
            <xsl:sort select="@name"/>
          </xsl:apply-templates>
        </xsl:when>
        <xsl:when test="$sort-by = 'revname'">
          <xsl:apply-templates>
            <xsl:sort select="name(self::node())"/>
            <xsl:sort select="@name" order="descending"/>
          </xsl:apply-templates>
        </xsl:when>
        <xsl:otherwise>
          <xsl:apply-templates>
            <xsl:sort select="name(self::node())"/>
            <xsl:sort select="@name"/>
          </xsl:apply-templates>
        </xsl:otherwise>
      </xsl:choose>
    </table>
  </xsl:template>
  
  <xsl:template match="directory">
    <xsl:variable name="name" select="@name"/>
    <xsl:variable name="type" select="@type"/>
    
    <tr class="directory">
      <td>
        <a href="{$command}{$name}?c=browse"><xsl:value-of select="@name"/></a>
      </td>
      <td><xsl:value-of select="@modified"/></td>
      <td>
        <a href="{$command}{$name}?c=annotate">annotate</a>
      </td>
      <td>
        <a href="{$command}{$name}?c=patches">patches</a>
      </td>
    </tr>

    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="file">
    <xsl:variable name="name" select="@name"/>
    
    <tr class="file">
      <td><xsl:value-of select="@name"/></td>
      <td><xsl:value-of select="@modified"/></td>
      <td><a href="{$command}{$name}?c=annotate">annotate</a></td>
      <td><a href="{$command}{$name}?c=patches">patches</a></td>
    </tr>

    <xsl:apply-templates/>
  </xsl:template>
</xsl:stylesheet>
