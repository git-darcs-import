<?xml version="1.0" encoding="utf-8"?>

<!--
 template for converting darcs' `changes` output from XML to XHTML.  This
 template expects the following external variables:

   cgi-program    - path to the CGI executable, to place in links
   sort-by        - field to sort by
   stylesheet     - path to the CSS stylesheet

 the input XML must have the following structure, aside from what common.xslt
 expects:

 <changelog>
   <patch author="" date="" localdate="" inverted="" hash="">
     <name></name>
   </patch>
 </changelog>
-->
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:str="http://exslt.org/strings">
  
  <xsl:include href="common.xslt"/>

  <xsl:template match="path">
    <h1 class="target">
      patches applied to <span class="path"><a href="{$cgi-program}">projects</a></span> / <span class="path"><xsl:apply-templates/></span>
    </h1>
  </xsl:template>
  
  
  <xsl:template match="changelog">
    <table>
      <tr class="patches">
        <th>
          <a class="sort" href="{$command}?c=patches&amp;s=name">name</a>
          <a class="revsort" href="{$command}?c=patches&amp;s=revname"> (rev)</a>
        </th>
        <th>
          <a class="sort" href="{$command}?c=patches&amp;s=date">date</a>
          <a class="revsort" href="{$command}?c=patches&amp;s=revdate"> (rev)</a>
        </th>
        <th>
          <a class="sort" href="{$command}?c=patches&amp;s=author">author</a>
          <a class="revsort" href="{$command}?c=patches&amp;s=revauthor"> (rev)</a>
        </th>
        <th>
        </th>
      </tr>

      <xsl:choose>
        <xsl:when test="$sort-by = 'name'">
          <xsl:apply-templates>
            <xsl:sort select="name"/>
          </xsl:apply-templates>
        </xsl:when>
        <xsl:when test="$sort-by = 'revname'">
          <xsl:apply-templates>
            <xsl:sort select="name" order="descending"/>
          </xsl:apply-templates>
        </xsl:when>
        <xsl:when test="$sort-by = 'date'">
          <xsl:apply-templates>
            <xsl:sort select="@date"/>
          </xsl:apply-templates>
        </xsl:when>
        <xsl:when test="$sort-by = 'revdate'">
          <xsl:apply-templates>
            <xsl:sort select="@date" order="descending"/>
          </xsl:apply-templates>
        </xsl:when>
        <xsl:when test="$sort-by = 'author'">
          <xsl:apply-templates>
            <xsl:sort select="@author"/>
          </xsl:apply-templates>
        </xsl:when>
        <xsl:when test="$sort-by = 'revauthor'">
          <xsl:apply-templates>
            <xsl:sort select="@author" order="descending"/>
          </xsl:apply-templates>
        </xsl:when>
        <xsl:otherwise>
          <!-- use the repository's natural order -->
          <xsl:apply-templates/>
        </xsl:otherwise>
      </xsl:choose>
    </table>
  </xsl:template>

  <xsl:template match="patch">
    <xsl:variable name="author" select="@author"/>
    <xsl:variable name="hash"   select="@hash"/>
    <xsl:variable name="name"   select="name"/>
    <xsl:variable name="repo"   select="/darcs/@repository"/>

    <xsl:variable name="created-as" select="/darcs/changelog/created_as"/>
    <xsl:variable name="creator-hash" select="$created-as/patch/@hash"/>
    <xsl:variable name="original-name" select="$created-as/@original_name"/>

    <xsl:variable name="annotate-href">
      <xsl:value-of select="$command"/>?c=annotate&amp;p=<xsl:value-of select="$hash"/>

      <xsl:if test="$creator-hash">&amp;ch=<xsl:value-of select="$creator-hash"/></xsl:if>
      <xsl:if test="$original-name">&amp;o=<xsl:value-of select="$original-name"/></xsl:if>
    </xsl:variable>
    
    <tr class="patch">
      <td><a href="{$annotate-href}"><xsl:value-of select="name"/></a></td>
      <td><xsl:value-of select="@local_date"/></td>
      <td>
        <a href="mailto:{$author}"><xsl:value-of select="$author"/></a>
      </td>
      <td>
        <a href="{$cgi-program}/{$repo}/?c=annotate&amp;p={$hash}">detail</a>
      </td>
    </tr>
    <xsl:apply-templates/>
  </xsl:template>

  <!-- ignore <created_as>, <name> and <comment> children of <patch> -->
  <xsl:template match="created_as"/>
  <xsl:template match="name"/>
  <xsl:template match="comment"/>
</xsl:stylesheet>
