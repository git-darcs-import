<?xml version="1.0" encoding="utf-8"?>

<!--
 template for converting selected content of errors.xml XML to XHTML. This
 template expects the following external variables:

   error-type     - the type of error
   stylesheet     - path to the CSS stylesheet

 the input XML must have the following structure

 <errors>
   <error type="" title="">
     ...
   </error>
 </errors>
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="error[@type = $error-type]">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
      <head>
        <title>
          <xsl:value-of select="@title"/>
        </title>
        <link rel="stylesheet" href="{$stylesheet}"/>
      </head>

      <body>
        <xsl:copy-of select="*"/>
      </body>
    </html>
  </xsl:template>

  <!-- ignore errors that don't match -->
  <xsl:template match="error"/>
</xsl:stylesheet>
