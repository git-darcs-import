<?xml version="1.0" encoding="utf-8"?>

<!--
 template for displaying a list of available repositories, used when a
 CGI request has no repository path information.  This template expects
 the following external variables:

   cgi-program    - path to the CGI executable, to place in links
   stylesheet     - path to the CSS stylesheet

 the input XML must have the following structure:

 <darcs repository="">
   <repositories>
     <repository name=""/>
   </repositories>
 </darcs>
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:variable name="command">
    <xsl:value-of select="$cgi-program"/>/<xsl:value-of select="/darcs/@target"/>
  </xsl:variable>
  
  <xsl:template match="/darcs">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
      <head>
        <title>darcs repository viewer</title>
        <link rel="stylesheet" href="{$stylesheet}"/>
      </head>

      <body>
        <h1 class="target">Available Repositories</h1>

        <table>
          <xsl:apply-templates/>
        </table>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="repositories">
    <xsl:apply-templates>
      <xsl:sort select="name(self::node())"/>
      <xsl:sort select="@name"/>
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="repositories/repository">
    <xsl:variable name="repository"  select="@name"/>

    <tr>
      <td>
        <a href="{$cgi-program}/{$repository}/?c=browse">
          <xsl:value-of select="$repository"/>
        </a>
      </td>
    </tr>
  </xsl:template>
</xsl:stylesheet>
