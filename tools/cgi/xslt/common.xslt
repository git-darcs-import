<!--
 templates fragments shared by all templates.  This template expects
 the following external variables:

   cgi-program    - path to the CGI executable, to place in links
   stylesheet     - path to the CSS stylesheet

 the input XML must have the following structure, plus any elements
 that are not common to all templates:

 <darcs repository="">
   <path>
     <element full-path=""></element>
   </path>

   ...
 
 </darcs>
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:variable name="command">
    <xsl:value-of select="$cgi-program"/>/<xsl:value-of select="/darcs/@target"/>
  </xsl:variable>

  <xsl:variable name="repo" select="/darcs/@repository"/>
  
  <xsl:template match="/darcs">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
      <head>
        <title>darcs repository</title>
        <link rel="stylesheet" href="{$stylesheet}"/>
        <link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="{$command}?c=rss" />
      </head>

      <body>
        <xsl:apply-templates/>

        <form action="{$command}" method="get">
          <p>
            <input class="patches" type="submit" name="c" value="patches"/>
            
            <a href="{$command}?c=rss">rss</a>
            
            <span class="version">
              <a class="home" href="http://darcs.net/">darcs.cgi</a>
              v1.0
            </span>
          </p>
        </form>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="path/directory">
    <xsl:variable name="full-path"  select="@full-path"/>
    
    <a href="{$cgi-program}{$full-path}?c=browse">
      <xsl:apply-templates/>
    </a> /
  </xsl:template>

  <xsl:template match="path/file">
    <xsl:apply-templates/>
  </xsl:template>
  
</xsl:stylesheet>
