#!/bin/sh

set -ev

REPODIR=`pwd`

echo I think your repository is $REPODIR

# Building clean copy of franchise
rm -rf /tmp/franchise-test
darcs get http://darcs.net/repos/franchise /tmp/franchise-test
cd /tmp/franchise-test
runghc Setup.hs configure --prefix=$HOME --user
runghc Setup.hs install

# Get clean copy of darcs
rm -rf /tmp/darcs-franchise
darcs get $REPODIR /tmp/darcs-franchise
cd /tmp/darcs-franchise

# Test franchise build of darcs
runghc Setup.hs configure
runghc Setup.hs build -j4

if test !"wine-1.0" = `wine --version`; then
    echo Not running wine test, since wine is too old maybe.
    exit 0
fi

echo Testing cross-compile of darcs on windows

# Build clean copy of franchise under wine
rm -rf /tmp/franchise-test
darcs get http://darcs.net/repos/franchise /tmp/franchise-test
cd /tmp/franchise-test
wine runghc Setup.hs configure
wine runghc Setup.hs install

# Get another clean copy of darcs
rm -rf /tmp/darcs-windows
darcs get $REPODIR /tmp/darcs-windows
cd /tmp/darcs-windows

# Build darcs under wine
wine runghc Setup.hs configure
wine runghc Setup.hs build -j4
