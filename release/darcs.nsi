
Name "David's Advanced Version Control System version ${VERSION}"

SetCompressor lzma
OutFile "darcs-${VERSION}.exe"
;XPStyle on
ShowInstDetails show
ShowUninstDetails show

LicenseData "COPYING"
InstallDir $PROGRAMFILES\Darcs

Page license
Page components
Page directory
Page instfiles
UninstPage uninstConfirm
UninstPage instfiles

Section "Darcs executable"
    SetOverwrite on
    SetOutPath $INSTDIR

    SetDetailsPrint listonly

    File darcs.exe
SectionEnd

Section "Manual"
    SetOverwrite on
    SetOutPath $INSTDIR\manual

    SetDetailsPrint textonly
    DetailPrint "Copying manual"
    SetDetailsPrint listonly

    File manual\*
SectionEnd

;Section "Examples"
;    SetOverwrite on
;    SetOutPath $INSTDIR\examples
;
;    SetDetailsPrint textonly
;    DetailPrint "Copying examples"
;    SetDetailsPrint listonly
;
;    File darcs.exe
;SectionEnd

Section "Uninstall and registry entries"
    SetOverwrite on
    SetOutPath $INSTDIR

    WriteRegExpandStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Darcs" "UninstallString" '"$INSTDIR\uninstall.exe"'
    WriteRegExpandStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Darcs" "InstallLocation" "$INSTDIR"
    WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Darcs" "DisplayName" "David's Advanced Revision Control System ${VERSION}"
    WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Darcs" "DisplayVersion" "${VERSION}"
    WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Darcs" "URLInfoAbout" "http://abridgegame.org/darcs/"
    WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Darcs" "NoModify" "1"
    WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Darcs" "NoRepair" "1"

    WriteUninstaller $INSTDIR\uninstall.exe
SectionEnd

Section "Startmenu shortcuts"
    CreateDirectory $SMPROGRAMS\Darcs
    CreateShortCut "$SMPROGRAMS\Darcs\License.lnk" "$WINDIR\notepad.exe" "$INSTDIR\COPYING"
    WriteINIStr "$SMPROGRAMS\Darcs\Darcs site.url" "InternetShortcut" "URL" "http://abridgegame.org/darcs/"

    IfFileExists $INSTDIR\manual\*.* manual nomanual
    manual:
    WriteINIStr "$SMPROGRAMS\Darcs\Darcs manual.url" "InternetShortcut" "URL" "$INSTDIR\manual\darcs.html"
    nomanual:
    IfFileExists $INSTDIR\uninstall.exe uninst nouninst
    uninst:
    CreateShortCut "$SMPROGRAMS\Darcs\Uninstall Darcs.lnk" "$INSTDIR\uninstall.exe"
    nouninst:
SectionEnd

Section -always
    SetOverwrite on
    SetOutPath $INSTDIR
    File COPYING
SectionEnd

Section Uninstall
    SetDetailsPrint textonly
    DetailPrint "Uninstalling Darcs"
    SetDetailsPrint listonly

    DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Darcs"

    Delete $INSTDIR\darcs.exe
    Delete $INSTDIR\COPYING
    Rmdir /r $INSTDIR\examples
    Rmdir /r $INSTDIR\manual
    Rmdir /r $SMPROGRAMS\Darcs
    Rmdir $INSTDIR
SectionEnd


