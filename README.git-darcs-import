git-darcs-import
Yet another darcs to git converter.
(Tested under Linux, unlikely to work under Windows.)

Usage
  # git-darcs-import <source> [<destination>]

  pulls patches from the source repository and stores them in a git
  repository in the destination, in the 'git-darcs-import' branch. (If no
  destination is given, the current directory is used.)

  The same command can be used to update the destination repository.

  You should never commit anything to the 'git-darcs-import' branch.

To build
  - you may have to change the path to git in src/Git/Base.hs
  - autoconf, configure, make git-darcs-import
  - you can now copy the binary

Notes on design and internals
  - In addition to the git-darcs-import branch, git-darcs-import stores a
    list of patches together with their id in .git/darcs-inventory.
  - git does not track directories, while darcs does. To avoid problems
    with git removing empty directories that darcs still needs, a file
    '.git-darcs-dir' is created in each subdirectory of the git repository.
  - git-darcs-import creates an empty commit before processing any darcs
    patches. this simplifies the handling of commit parents, and allows it to
    process tags at the very start of a darcs repository
  - currently git-darcs-import uses a rather strict check to compare
    repositories -- the destination's inventory must be a /prefix/ of
    the source's.

Random notes
  - in case of conflicting patches, git's diff will not agree with the
    original patch - the conflicting changes will be missing.
  - competition:
       darcs2git:    http://repo.or.cz/w/darcs2git.git?a=shortlog
                     uses git-fast-import.
       darcs-to-git: http://git.sanityinc.com/?p=darcs-to-git.git
                     born out of frustration over darcs2git:
                     http://www.sanityinc.com/articles/converting-darcs-repositories-to-git

Credits
  David Roundy and the darcs contributors, for darcs
  Linus Torvalds, Junio Hamano and the git contributors, for git

Enjoy,

Bertram Felgenhauer
