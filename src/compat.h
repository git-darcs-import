
#include <sys/stat.h>
#include <string.h>
#include <stdlib.h>

int open_read(const char *fname);
int open_write(const char *fname);
int smart_wait(int pid);
int get_errno();

int execvp_no_vtalarm(const char *file, char *const argv[]);

