const char *libwww_request_url(const char *url,
                               const char *filename,
                               int cache_time);

const char *libwww_wait_next_url();

const char *libwww_last_url();

void libwww_enable_debug();
