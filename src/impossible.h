import qualified HTTP as HTTP_
import qualified Darcs.Bug as Bug_

#define darcsBug (\imp_funny_name -> imp_funny_name (__FILE__,__LINE__,__TIME__,__DATE__))

#define bug        (darcsBug (Bug_._bug HTTP_.fetchUrl))
#define impossible (darcsBug (Bug_._impossible HTTP_.fetchUrl))
#define fromJust   (darcsBug (Bug_._fromJust HTTP_.fetchUrl))
#define bugDoc     (darcsBug (Bug_._bugDoc HTTP_.fetchUrl))
