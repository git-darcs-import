-- |
-- Module      : Git.Metadata
-- Copyright   : (c) 2008 Bertram Felgenhauer
-- License     : GPL2
--
-- Maintainer  : Bertram Felgenhauer <int-e@gmx.de>
-- Stability   : experimental
-- Portability : ghc
--
-- convert from darcs to git metadata

module Git.Metadata (
    extractGitInfo,
    GitInfo (..),
) where

import Darcs.Patch.Info (
    PatchInfo, pi_author, pi_name, pi_tag, pi_date, pi_log, make_filename)
import System.Time (calendarTimeToString)

data GitInfo = GitInfo {
    gitAuthor  :: String,       -- author name
    gitAEmail  :: String,       -- author email
    gitTag     :: Maybe String, -- tag name (Nothing for normal patches)
    gitDate    :: String,       -- patch date
    gitMessage :: String        -- patch name and message
}

extractGitInfo :: PatchInfo -> GitInfo
extractGitInfo pip = GitInfo {
    gitAuthor  = fst (extractAuthor pip),
    gitAEmail  = snd (extractAuthor pip),
    gitTag     = extractTag pip,
    gitDate    = extractDate pip,
    gitMessage = extractMessage pip
}

-- split "Foo <bar@what>" into "Foo" and "bar@what".
-- (dirty implementation - can be improved, but mostly works)
extractAuthor :: PatchInfo -> (String, String)
extractAuthor pip = let
    m = pi_author pip
    ms = words (filter (/= '\'') m)
    (name, email) = span (\word -> head word /= '<') ms
  in
    if null email && length name == 1 && '@' `elem` head name then ("", "<" ++ head name ++ ">") else
    (if null name then "nobody" else unwords name, if null email then "<>" else unwords email)

-- derive a tag name from a tag message. basically filter allowed characters.
extractTag :: PatchInfo -> Maybe String
extractTag = fmap (concatMap tagChar) . pi_tag where
    tagChar c | '0' <= c && c <= '9' ||
                'a' <= c && c <= 'z' ||
                'A' <= c && c <= 'Z' ||
                c `elem` validChars = c : []
    tagChar ' ' = "_"
    tagChar _   = ""
    validChars = ".<>-"

extractDate :: PatchInfo -> String
extractDate = calendarTimeToString . pi_date

extractMessage :: PatchInfo -> String
extractMessage pip = unlines
    (pi_name pip : "" : pi_log pip ++ ["", make_filename pip])
