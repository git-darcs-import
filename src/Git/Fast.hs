-- |
-- Module      : Git.Fast
-- Copyright   : (c) 2008 Bertram Felgenhauer
-- License     : GPL2
--
-- Maintainer  : Bertram Felgenhauer <int-e@gmx.de>
-- Stability   : experimental
-- Portability : ghc
--
-- Functions for applying patches to git repositories.
--
-- Use git-fast-import for importing stuff, and a Slurpy tree for tracking
-- commits.
--
-- This module also defines how git-darcs-import stores its metadata in the
-- target repository, and how patch meta information is converted.
--

{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Git.Fast (
    Git,
    runGit,
    getInventory,
    failure,
    applyAndCommit
) where

import Control.Monad.State.Strict (evalStateT)
import Control.Monad.Trans (liftIO, MonadIO)
import Control.Monad (when, liftM2)
import System.Process (runProcess)
import System.FilePath ((</>), FilePath)
import System.Directory (doesDirectoryExist) -- , createDirectory)

import Darcs.SlurpDirectory (SlurpMonad, slurp, withSlurpy)
import Darcs.Patch (RepoPatch, apply)
import Darcs.Utils (withCurrentDirectory)
import Darcs.IO (WriteableDirectory (..), ReadableDirectory (..))
import Darcs.Patch.Core (Named (..))
import Darcs.Patch.Info (make_filename)
import Darcs.Patch.FileName (fp2fn, (///), FileName)
import Git.Base
import Git.Metadata (GitInfo (..), extractGitInfo)
import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as BC
import ByteStringUtils (linesPS, unlinesPS)

-- | location of inventory file
-- format:
-- 20051128181018-c2a52-a799f1a83580ad95053914b2171e790425b37dc6.gz :<mark>
inventoryFile :: FilePath
inventoryFile =  ".git" </> "darcs-inventory"

-- | a file placed into each directory, so git can keep track of it.
directoryMarker :: FileName
directoryMarker = fp2fn ".git-darcs-dir"

-- | the branch that we create
branch :: FilePath
branch = "refs/heads/git-darcs-import"

-- | create a checkpoint after this many files have been touched
cpCount :: Int
cpCount = 10000

-- | initialise empty repository. creates empty marksFile and inventoryFile.
makeRepository :: IO ()
makeRepository = do
    pid <- runProcess gitBinary ["init-db"]
               Nothing Nothing Nothing Nothing Nothing
    checkProcessExitCode "Failed to initialise git repo" pid
    B.writeFile inventoryFile B.empty
    B.writeFile marksFile B.empty
--    createDirectory "_darcs"
--    createDirectory "_darcs/prefs"

-- | the inventory might be ahead of git's actual commits - repair.
checkInventory :: IO Int
checkInventory = do
    inv <- liftIO $ B.readFile inventoryFile
    mrk <- liftIO $ B.readFile marksFile
    let is = tail (reverse (linesPS inv))
        is' = dropWhile (\l -> snd (splitInventoryLine l) > lastMark) is
        ms = linesPS mrk
        lastMark = length ms - 1
        inv' = unlinesPS (reverse (B.empty : is'))
        m' | null is  = 1
           | otherwise = snd (splitInventoryLine (head is'))
        commit = BC.unpack (snd (splitMarkLine (ms !! (m'-1))))
    when (length is' < length is) $ do
        putStrLn $ "resetting to " ++ commit
        B.writeFile inventoryFile inv'
        pid <- runProcess "git" ["checkout", "-f", "-q", commit]
                   Nothing Nothing Nothing Nothing Nothing
        checkProcessExitCode ("failed to check out tip (" ++ commit ++ ")") pid
        return ()
    return m'

-- | parse a line from our inventory file. return darcs hash and mark
-- number.
splitInventoryLine :: B.ByteString -> (B.ByteString, Int)
splitInventoryLine ps = (B.take 64 ps, read . BC.unpack . B.drop 66 $ ps)

-- | parse a line from git-fast-import's marks file. return mark number
-- and commit
splitMarkLine :: B.ByteString -> (Int, B.ByteString)
splitMarkLine ps =
   let l = B.length ps
   in  (read . tail . BC.unpack . B.take (l-41) $ ps, B.drop (l-40) ps)

-- | run a Git action in a repository. initialises the repository if it's fresh.
runGit :: Git b     -- ^ pre initialisation hook, for setting options
       -> FilePath  -- ^ Root of repository
       -> Git a     -- ^ action to run on repository
       -> IO a
runGit pre root act = withCurrentDirectory root $ do
    t <- liftIO $ doesDirectoryExist ".git"
    when (not t) makeRepository
    -- read working directory
    tr <- slurp "."
    mrk <- if t then do
        checkInventory
      else
        return 1
    let initialise = do
            startGitFastImport
            when (not t) $ do
                -- create a dummy commit at the start of the repository
                let message = "New empty repository"
                fastGitCommand [
                    "commit " ++ branch,
                    "mark :1",
                    "author git-darcs-import <> Thu, 01 Jan 1970 00:00:00 UTC",
                    "committer git-darcs-import <> Thu, 01 Jan 1970 00:00:00 UTC",
                    "data " ++ show (length message),
                    message]
        finish = stopGitFastImport >> checkout branch
    let initState = RWState { mark = mrk, tree = tr, touched = 0,
            gitPipe = error "gitPipe", gitPid = error "gitPid",
            verbose = 0 }
    flip evalStateT initState $ unGit $
        pre >> initialise >> liftM2 const act finish

-- | read the inventory, and return a list of pairs of
-- mark and darcs patch file
getInventory :: Git [(B.ByteString, Int)]
getInventory = liftIO $ do
    inv <- B.readFile inventoryFile
    return $ map splitInventoryLine (init (linesPS inv))

-- | apply a patch and add it to the inventory. the repository must be clean.
-- it's an error to apply a patch that is already in the repository.
applyAndCommit :: RepoPatch p => Named p -> Git ()
applyAndCommit (NamedP pip _ p) = do
    m <- getMark
    let GitInfo {
            gitAuthor = aName,
            gitAEmail = aEmail,
            gitTag = aTag,
            gitDate = aDate,
            gitMessage = message
        } = extractGitInfo pip
        authorLine = aName ++ " " ++ aEmail ++ " " ++ aDate
    case aTag of
        Just tag -> do
            fastGitCommand [
                "tag " ++ tag,
                "from :" ++ show m,
                "tagger " ++ authorLine,
                "data " ++ show (length message),
                message]
        Nothing -> do
            fastGitCommand [
                "commit " ++ branch,
                "mark :" ++ show (m + 1),
                "author " ++ authorLine,
                "committer " ++ authorLine,
                "data " ++ show (length message),
                message,
                "from :" ++ show m]
            setMark (m+1)
    apply [] p
    t <- getTouched
    when (t > cpCount) $ do
        liftIO $ putStrLn "\nCreating checkpoint..."
{-
        -- this way we could restart the import process, hmm
        stopGitFastImport
        startGitFastImport
-}
        fastGitCommand ["checkpoint"]
        setTouched 0
    liftIO $ appendFile inventoryFile $ make_filename pip ++ " :" ++ show (m+1) ++ "\n"
    return ()

-- | run an action on the working tree
liftSlurpMonad :: SlurpMonad a -> Git a
liftSlurpMonad act = do
    t0 <- getTree
    case withSlurpy t0 act of
        Left  msg       -> failure msg
        Right (t1, res) -> do
            setTree t1
            return res

-- read the working tree - boring
instance ReadableDirectory Git where
    mInCurrentDirectory = error "Git :: mInCurrentDirectory"
    mDoesDirectoryExist d = liftSlurpMonad $ mDoesDirectoryExist d
    mDoesFileExist f = liftSlurpMonad $ mDoesFileExist f
    mGetDirectoryContents = liftSlurpMonad $ mGetDirectoryContents
    mReadFilePS f = liftSlurpMonad $ mReadFilePS f
    mReadFilePSs f = liftSlurpMonad $ mReadFilePSs f

-- write to the working tree - notify the git-fast-import worker
instance WriteableDirectory Git where
    mWithCurrentDirectory = error "Git :: mWithCurrentDirectory"
    mSetFileExecutable = error "Git :: mSetFileExecutable"
    mWriteFilePS f ps = do
        liftSlurpMonad $ mWriteFilePS f ps
        gitWriteFilePS f ps
    mCreateDirectory f = do
        liftSlurpMonad $ mCreateDirectory f
        mWriteFilePS (f /// directoryMarker) B.empty
    mRemoveDirectory f = do
        mRemoveFile (f /// directoryMarker)
        liftSlurpMonad $ mRemoveDirectory f
    mRemoveFile f = do
        liftSlurpMonad $ mRemoveFile f
        gitRemoveFile f
    mRename f1 f2 = do
        -- some old patches have moves where the source file doesn't exist
        exF <- mDoesFileExist f1
        exD <- mDoesDirectoryExist f1
        when (exF || exD) $ gitRename f1 f2
        liftSlurpMonad $ mRename f1 f2
