-- |
-- Module      : Git.Base
-- Copyright   : (c) 2008 Bertram Felgenhauer
-- License     : GPL2
--
-- Maintainer  : Bertram Felgenhauer <int-e@gmx.de>
-- Stability   : experimental
-- Portability : ghc
--
-- Git monad. Handles state, and executing the git-fast-import program
--

{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Git.Base (
    Git (unGit),
    RWState (..),
    failure,
    fastGitCommand,
    startGitFastImport,
    stopGitFastImport,
    gitWriteFilePS,
    gitRemoveFile,
    gitRename,
    checkout,
    gitBinary,
    marksFile,
    modifyVerbose,
    getVerbose,
    getTouched,
    setTouched,
    getMark,
    setMark,
    getTree,
    setTree,
    checkProcessExitCode,
) where

import Control.Monad.State.Strict (StateT, gets, modify)
import Control.Monad.Trans (MonadIO, liftIO)
import Control.Monad (MonadPlus, when)
import System.Exit (exitFailure, ExitCode (..))
import System.IO (Handle, hPutStr, hClose, hPutStrLn, stderr)
import System.IO.Unsafe (unsafePerformIO)
import System.Process (ProcessHandle, runProcess, waitForProcess)
import System.Posix (createPipe, setFdOption, fdToHandle, FdOption (..))
import System.FilePath ((</>), FilePath)
import System.Directory (findExecutable)

import Darcs.SlurpDirectory (Slurpy)
import Darcs.Patch.FileName (FileName, fn2fp)
import qualified Data.ByteString as B

-- | path to git executable
gitBinary :: FilePath
gitBinary = unsafePerformIO $ do -- FIXME unsafePerformIO, this can be done better 
              e <- findExecutable "git"
              case e of
                Just ex -> return ex
                Nothing -> fail "git executable not found in PATH"

-- | file that maps marks to git commits (created by git-fast-import)
marksFile :: FilePath
marksFile =  ".git" </> "darcs-marks"

data RWState = RWState {
    gitPipe :: Handle,         -- ^ pipe to git-fast-import
    gitPid  :: ProcessHandle,  -- ^ process handle of git-fast-import
    tree    :: !Slurpy,        -- ^ current working tree
    mark    :: !Int,           -- ^ mark of latest commit
    touched :: !Int,           -- ^ count modified files
    verbose :: !Int            -- ^ be verbose
}

-- | Git monad
newtype Git a = Git { unGit :: StateT RWState IO a }
    deriving (Monad, MonadPlus, MonadIO, Functor)

-- | pass some commands to git-fast-import
fastGitCommand :: [String] -> Git ()
fastGitCommand cmd = do
    v <- getVerbose
    when (v > 1) $ liftIO $ print cmd
    h <- getGitPipe
    liftIO $ hPutStr h $ unlines cmd

-- | start git-fast-import
-- git-fast-import must be running for 'fastGitCommand' to work
startGitFastImport :: Git ()
startGitFastImport = do
    v <- getVerbose
    (iw, pid) <- liftIO $ do
        -- set up pipe
        (irFd, iwFd) <- createPipe
        setFdOption iwFd CloseOnExec True
        ir <- fdToHandle irFd
        iw <- fdToHandle iwFd
        -- fdToHandle sets O_NONBLOCK, which confuses git
        setFdOption irFd NonBlockingRead False
        -- run git-fast-import
        pid <- runProcess gitBinary
            (["fast-import",
             "--date-format=rfc2822",
             "--active-branches=1",
             "--force",
             "--import-marks=" ++ marksFile,
             "--export-marks=" ++ marksFile] ++
            ["--quiet" | v == 0])
            Nothing Nothing (Just ir) Nothing Nothing
        return (iw, pid)
    Git $ modify (\s -> s { gitPid = pid, gitPipe = iw })

-- | stop git-darcs-import
stopGitFastImport :: Git ()
stopGitFastImport = do
    h <- getGitPipe
    git <- getGitPid
    liftIO $ do
        hClose h
        checkProcessExitCode "git-fast-import failed." git

-- | check out final tree
checkout :: String -> Git ExitCode
checkout branch = liftIO $ do
    pid <- runProcess gitBinary ["checkout", "-f", "-q", branch]
               Nothing Nothing Nothing Nothing Nothing
    waitForProcess pid

-- | abort due to some error
failure :: String -> Git a
failure msg = do
    liftIO $ putStrLn msg
    stopGitFastImport
    liftIO $ exitFailure

gitWriteFilePS :: FileName -> B.ByteString -> Git ()
gitWriteFilePS f ps = do
    fastGitCommand ["M 644 inline " ++ quoteFN f,
                    "data " ++ show (B.length ps)]
    h <- getGitPipe
    liftIO $ B.hPut h ps
    incTouched

gitRemoveFile :: FileName -> Git ()
gitRemoveFile f = fastGitCommand ["D " ++ quoteFN f]

gitRename :: FileName -> FileName -> Git ()
gitRename f1 f2 = fastGitCommand ["R " ++ quoteFN f1 ++ " " ++ quoteFN f2]

-- FIXME:
-- should quote \", \\, \  and \n at least
quoteFN :: FileName -> String
quoteFN f = case fn2fp f of
    '.' : '/' : f' -> f'
    f' -> f'

-- fixme: this doesn't belong here
-- | wait for a process that is expected to terminate successfully
--   print a message and abort if it fails.
checkProcessExitCode :: String -> ProcessHandle -> IO ()
checkProcessExitCode msg pid = do
    ec <- waitForProcess pid
    case ec of
        ExitSuccess -> return ()
        ExitFailure c -> do
            hPutStrLn stderr $ msg ++ " (exit code " ++ show c ++ ")"
            exitFailure

-- trivial setters and getters for our state
-- | modify verbosity level
modifyVerbose :: (Int -> Int) -> Git ()
modifyVerbose f = Git $ modify (\s -> s { verbose = f (verbose s) })

-- | get verbosity level
getVerbose :: Git Int
getVerbose = Git $ gets verbose

-- | get pipe to git-fast-import process
getGitPipe :: Git Handle
getGitPipe = Git $ gets gitPipe

-- | get pid of git-fast-import process
getGitPid :: Git ProcessHandle
getGitPid = Git $ gets gitPid

-- | get current mark (see Git.Fast)
getMark :: Git Int
getMark = Git $ gets mark

-- | set current mark (see Git.Fast)
setMark :: Int -> Git ()
setMark m = Git $ modify (\s -> s { mark = m })

-- | get number of touched files (see Git.Fast)
getTouched :: Git Int
getTouched = Git $ gets touched

-- | set number of touched files (see Git.Fast)
setTouched :: Int -> Git ()
setTouched t = Git $ modify (\s -> s { touched = t })

-- | increment number of touched files (see Git.Fast)
incTouched :: Git ()
incTouched = Git $ modify (\s -> s { touched = touched s + 1 })

-- | get current repo contents (see Git.Fast)
getTree :: Git Slurpy
getTree = Git $ gets tree

-- | set current repo contents (see Git.Fast)
setTree :: Slurpy -> Git ()
setTree t = Git $ modify (\s -> s { tree = t })
