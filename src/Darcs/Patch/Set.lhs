%  Copyright (C) 2003 David Roundy
%
%  This program is free software; you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation; either version 2, or (at your option)
%  any later version.
%
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
%
%  You should have received a copy of the GNU General Public License
%  along with this program; see the file COPYING.  If not, write to
%  the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
%  Boston, MA 02110-1301, USA.


\begin{code}
{-# OPTIONS_GHC -cpp #-}
{-# LANGUAGE CPP #-}

#include "gadts.h"

module Darcs.Patch.Set ( PatchSet, SealedPatchSet ) where

import Darcs.Hopefully ( PatchInfoAnd )
import Darcs.Ordered ( RL )
import Darcs.Sealed ( Sealed )

-- | A PatchSet is in reverse order, plus has information about which
-- tags are clean, meaning all patches applied prior to them are in
-- the tag itself, so we can stop reading at that point.  Just to
-- clarify, the first patch in a PatchSet is the one most recently
-- applied to the repo.
--
-- 'PatchSet's have the property that if
-- @
-- (info $ last $ head a) == (info $ last $ head b)
-- @
-- then @(tail a)@ and @(tail b)@ are identical repositories
--
-- Questions:
--
-- Does this mean that in a patch set such as @[[a b t1 c d e t2][f g
-- t3] [h i]]@, t1, t2 and t3 are tags, and t2 and t3 are clean?
--
-- Can we have PatchSet with length at least 3?
-- Florent
type PatchSet p C(x) = RL (RL (PatchInfoAnd p)) C(() x)

type SealedPatchSet p = Sealed (RL (RL (PatchInfoAnd p)) C(()))
\end{code}

