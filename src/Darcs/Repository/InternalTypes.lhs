%  Copyright (C) 2006-2007 David Roundy
%
%  This program is free software; you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation; either version 2, or (at your option)
%  any later version.
%
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
%
%  You should have received a copy of the GNU General Public License
%  along with this program; if not, write to the Free Software Foundation,
%  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

\begin{code}
{-# OPTIONS_GHC -cpp -fglasgow-exts #-}
{-# LANGUAGE CPP #-}
-- , KindSignatures #-}

#include "gadts.h"

module Darcs.Repository.InternalTypes ( Repository(..), RepoType(..), Pristine(..)
                                      , extractCache
                                      ) where

import Darcs.Repository.Cache ( Cache )
import Darcs.Flags ( DarcsFlag )
import Darcs.Repository.Format ( RepoFormat )

data Pristine
  = NoPristine !String
  | PlainPristine !String
  | HashedPristine
    deriving ( Show )

data Repository (p :: * C(-> * -> *)) C(recordedstate unrecordedstate tentativestate) = Repo !String ![DarcsFlag] !RepoFormat !(RepoType p) deriving ( Show )

data RepoType (p :: * C(-> * -> *)) = DarcsRepository !Pristine Cache deriving ( Show )

extractCache :: Repository p C(r u t) -> Cache
extractCache (Repo _ _ _ (DarcsRepository _ c)) = c
\end{code}
