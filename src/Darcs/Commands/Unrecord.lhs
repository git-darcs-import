%  Copyright (C) 2002-2005 David Roundy
%
%  This program is free software; you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation; either version 2, or (at your option)
%  any later version.
%
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
%
%  You should have received a copy of the GNU General Public License
%  along with this program; see the file COPYING.  If not, write to
%  the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
%  Boston, MA 02110-1301, USA.

\subsection{darcs unrecord}
\label{unrecord}
\begin{code}
{-# OPTIONS_GHC -cpp #-}
{-# LANGUAGE CPP #-}

module Darcs.Commands.Unrecord ( unrecord, unpull, obliterate, rempatch, get_last_patches ) where
import Control.Monad ( when )
import System.Exit ( exitWith, ExitCode( ExitSuccess ) )

import Darcs.SlurpDirectory ( wait_a_moment )
import Darcs.Hopefully ( hopefully, info )
import Darcs.Commands ( DarcsCommand(..), nodefaults, loggers, command_alias )
import Darcs.Arguments ( DarcsFlag( Verbose ),
                         working_repo_dir, nocompress, definePatches,
                        match_several_or_last, deps_sel,
                        ignoretimes,
                        all_interactive, umask_option,
                      )
import Darcs.Match ( first_match, match_first_patchset, match_a_patchread )
import Darcs.Repository ( PatchSet, PatchInfoAnd, withGutsOf,
                          withRepoLock, ($-), slurp_recorded,
                    tentativelyRemovePatches, finalizeRepositoryChanges,
                    tentativelyAddToPending,
                    applyToWorking,
                    get_unrecorded, read_repo, amInRepository,
                    sync_repo,
                  )
import Darcs.Patch ( Patchy, RepoPatch, Named, invert, patch2patchinfo, commutex, effect )
import Darcs.Ordered ( RL(..), (:<)(..), (:>)(..), (:\/:)(..), (+<+),
                             unsafeRL, unsafeUnRL, mapFL_FL, nullFL,
                             concatRL, reverseRL, mapRL, mapRL_RL )
import Darcs.Patch.Depends ( deep_optimize_patchset, get_common_and_uncommon )
import Darcs.SelectChanges ( with_selected_last_changes_reversed )
import Darcs.Progress ( debugMessage )
import Darcs.Sealed ( Sealed(..) )
#include "impossible.h"
\end{code}
\begin{code}
unrecord_description :: String
unrecord_description =
 "Remove recorded patches without changing the working copy."
\end{code}

\options{unrecord}

\haskell{unrecord_help}

Unrecord can be thought of as undo-record.
If a record is followed by an unrecord, everything looks like before
the record; all the previously unrecorded changes are back, and can be
recorded again in a new patch. The unrecorded patch however is actually
removed from your repository, so there is no way to record it again to get
it back.\footnote{The patch file itself is not actually deleted, but its
context is lost, so it cannot be reliably read---your only choice would be
to go in by hand and read its contents.}.

If you want to remove
the changes from the working copy too (where they otherwise will show
up as unrecorded changes again), you'll also need to \verb!darcs revert!.
To do unrecord and revert in one go, you can use \verb!darcs obliterate!.

If you don't revert after unrecording, then the changes made by the
unrecorded patches are left in your working tree.  If these patches are
actually from another repository, interaction (either pushes or pulls) with
that repository may be massively slowed down, as darcs tries to cope with
the fact that you appear to have made a large number of changes that
conflict with those present in the other repository.  So if you really want
to undo the result of a \emph{pull} operation, use obliterate! Unrecord is
primarily intended for when you record a patch, realize it needs just one
more change, but would rather not have a separate patch for just that one
change.

\newcommand{\pullwarning}[1]{
\textbf{WARNING:} #1 should not be run when there is a possibility
that another user may be pulling from the same repository.  Attempting to do so
may cause repository corruption.}

\pullwarning{Unrecord}

\begin{options}
--from-match, --from-patch, --from-tag, --last
\end{options}

Usually you only want to unrecord the latest changes,
and almost never would you want to unrecord changes before a tag---you
would have to have unrecorded the tag as well to do that.
Therefore, and for efficiency, darcs only prompts you for the latest patches,
after some optimal tag.

If you do want to unrecord more patches in one go,
there are the \verb!--from! and \verb!--last! options
to set the earliest patch selectable to unrecord.

\begin{options}
--matches, --patches, --tags, --no-deps
\end{options}

The \verb!--patches!, \verb!--matches!, \verb!--tags!, and \verb!--no-deps!
options can be used to select which patches to unrecord, as described in
subsection~\ref{selecting}.

With these options you can specify
what patch or patches to be prompted for by unrecord.
This is especially useful when you want to unrecord patches with dependencies,
since all the dependent patches (but no others) will be included in the choices.
Or if you use \verb!--no-deps! you won't be asked about patches that can't be
unrecorded due to depending patches.

Selecting patches can be slow, so darcs cuts the search at the last
optimized tag. Use the \verb!--from! or \verb!--last! options to search
more or fewer patches.

\begin{code}
unrecord_help :: String
unrecord_help =
 "Unrecord does the opposite of record in that it makes the changes from\n"++
 "patches active changes again which you may record or revert later.  The\n"++
 "working copy itself will not change.\n"++
 "Beware that you should not use this command if you are going to\n"++
 "re-record the changes in any way and there is a possibility that\n"++
 "another user may have already pulled the patch.\n"
\end{code}
\begin{code}
unrecord :: DarcsCommand
unrecord = DarcsCommand {command_name = "unrecord",
                         command_help = unrecord_help,
                         command_description = unrecord_description,
                         command_extra_args = 0,
                         command_extra_arg_help = [],
                         command_command = unrecord_cmd,
                         command_prereq = amInRepository,
                         command_get_arg_possibilities = return [],
                         command_argdefaults = nodefaults,
                         command_advanced_options = [nocompress,umask_option],
                         command_basic_options = [match_several_or_last,
                                                 deps_sel,
                                                 all_interactive,
                                                 working_repo_dir]}
\end{code}
\begin{code}
unrecord_cmd :: [DarcsFlag] -> [String] -> IO ()
unrecord_cmd opts _ = withRepoLock opts $- \repository -> do
  let (logMessage,_,_) = loggers opts
  recorded <- slurp_recorded repository
  allpatches <- read_repo repository
  let patches = if first_match opts then get_last_patches opts allpatches
                                    else matchingHead opts allpatches
  with_selected_last_changes_reversed "unrecord" opts recorded
      (reverseRL patches) $
    \ (_ :> to_unrecord) -> do
       when (nullFL to_unrecord) $ do logMessage "No patches selected!"
                                      exitWith ExitSuccess
       when (Verbose `elem` opts) $
            logMessage "About to write out (potentially) modified patches..."
       definePatches to_unrecord
       withGutsOf repository $ do tentativelyRemovePatches repository opts $
                                                           mapFL_FL hopefully to_unrecord
                                  finalizeRepositoryChanges repository
       sync_repo repository
       logMessage "Finished unrecording."

get_last_patches :: RepoPatch p => [DarcsFlag] -> PatchSet p -> RL (PatchInfoAnd p)
get_last_patches opts ps =
  case get_common_and_uncommon (ps,p1s) of
  (_,us :\/: _) -> concatRL us
  where (Sealed p1s) = match_first_patchset opts ps

rempatch :: RepoPatch p => Named p -> PatchSet p -> PatchSet p
rempatch p (pps:<:ppss) =
    case patch2patchinfo p of
    pinfo -> if pinfo `elem` simple_infos
                  -- The following ugly hack is only safe because we don't use
                  -- the actual patches!
                  then (unsafeRL $ filter ((/= pinfo).info) $ unsafeUnRL pps) :<: ppss
                  else deep_optimize_patchset $
                       mapRL_RL (unsafeRL . filter ((/= pinfo).info) . unsafeUnRL) (pps:<:ppss)
    where simple_infos = init $ mapRL info pps
rempatch _ NilRL = impossible
\end{code}

\begin{code}
unpull_description :: String
unpull_description =
 "Opposite of pull; unsafe if patch is not in remote repository."
\end{code}

\begin{code}
unpull_help :: String
unpull_help =
 "Unpull completely removes recorded patches from your local repository.\n"++
 "The changes will be undone in your working copy and the patches will not be\n"++
 "shown in your changes list anymore.\n"++
 "Beware that if the patches are not still present in another repository you\n"++
 "will lose precious code by unpulling!\n"
\end{code}

\begin{code}
unpull :: DarcsCommand
unpull = (command_alias "unpull" obliterate)
                      {command_help = unpull_help,
                       command_description = unpull_description,
                       command_command = unpull_cmd}

unpull_cmd :: [DarcsFlag] -> [String] -> IO ()
unpull_cmd = generic_obliterate_cmd "unpull"
\end{code}



\subsection{darcs obliterate}

\begin{code}
obliterate_description :: String
obliterate_description =
 "Delete selected patches from the repository. (UNSAFE!)"
\end{code}

\begin{code}
obliterate_help :: String
obliterate_help =
 "Obliterate completely removes recorded patches from your local repository.\n"++
 "The changes will be undone in your working copy and the patches will not be\n"++
 "shown in your changes list anymore.\n"++
 "Beware that you can lose precious code by obliterating!\n"
\end{code}

\options{obliterate}

\haskell{obliterate_help}

Obliterate deletes a patch from the repository \emph{and} removes those
changes from the working directory.  It is therefore a \emph{very
dangerous} command.  When there are no local changes, obliterate is
equivalent to an unrecord followed by a revert, except that revert can be
unreverted.  In the case of tags, obliterate removes the tag itself, not
any other patches.

Note that unpull was the old name for obliterate. Unpull is still an
hidden alias for obliterate.

\pullwarning{Obliterate}

\begin{options}
--from-match, --from-patch, --from-tag, --last
\end{options}

Usually you only want to obliterate the latest changes, and almost never would
you want to obliterate changes before a tag---you would have to have obliterated
the tag as well to do that. Therefore, and for efficiency, darcs only
prompts you for the latest patches, after some optimal tag.

If you do want to obliterate more patches in one go, there are the
\verb!--from! and \verb!--last! options to set the earliest patch
selectable to obliterate.

\begin{options}
--matches, --patches, --tags, --no-deps
\end{options}

The \verb!--patches!, \verb!--matches!, \verb!--tags!, and \verb!--no-deps!
options can be used to select which patches to obliterate, as described in
subsection~\ref{selecting}.

With these options you can specify what patch or patches to be prompted for
by obliterate. This is especially useful when you want to obliterate patches with
dependencies, since all the dependent patches (but no others) will be
included in the choices. Or if you use \verb!--no-deps! you won't be asked
about patches that can't be obliterated due to depending patches.

Selecting patches can be slow, so darcs cuts the search at the last
optimized tag. Use the \verb!--from! or \verb!--last! options to search
more or fewer patches.

\begin{code}
obliterate :: DarcsCommand
obliterate = DarcsCommand {command_name = "obliterate",
                           command_help = obliterate_help,
                           command_description = obliterate_description,
                           command_extra_args = 0,
                           command_extra_arg_help = [],
                           command_command = obliterate_cmd,
                           command_prereq = amInRepository,
                           command_get_arg_possibilities = return [],
                           command_argdefaults = nodefaults,
                           command_advanced_options = [nocompress,ignoretimes,umask_option],
                           command_basic_options = [match_several_or_last,
                                                   deps_sel,
                                                   all_interactive,
                                                   working_repo_dir]}
obliterate_cmd :: [DarcsFlag] -> [String] -> IO ()
obliterate_cmd = generic_obliterate_cmd "obliterate"
\end{code}

\begin{code}
generic_obliterate_cmd :: String -> [DarcsFlag] -> [String] -> IO ()
generic_obliterate_cmd cmdname opts _ = withRepoLock opts $- \repository -> do
  let (logMessage,_,_) = loggers opts
  recorded <- slurp_recorded repository
  pend <- get_unrecorded repository
  allpatches <- read_repo repository
  let patches = if first_match opts then get_last_patches opts allpatches
                                    else matchingHead opts allpatches
  with_selected_last_changes_reversed cmdname opts recorded
      (reverseRL patches) $
    \ (_ :> ps) ->
    case commutex (pend :< effect ps) of
    Nothing -> fail $ "Can't "++ cmdname ++
               " patch without reverting some unrecorded change."
    Just (p_after_pending:<_) -> do
        when (nullFL ps) $ do logMessage "No patches selected!"
                              exitWith ExitSuccess
        definePatches ps
        withGutsOf repository $
                             do tentativelyRemovePatches repository opts (mapFL_FL hopefully ps)
                                tentativelyAddToPending repository opts $ invert $ effect ps
                                finalizeRepositoryChanges repository
                                debugMessage "Waiting a bit for timestamps to differ..."
                                wait_a_moment
                                debugMessage "Applying patches to working directory..."
                                applyToWorking repository opts (invert p_after_pending) `catch` \e ->
                                    fail ("Couldn't undo patch in working dir.\n" ++ show e)
        sync_repo repository
        logMessage $ "Finished " ++ present_participle cmdname ++ "."

matchingHead :: Patchy p => [DarcsFlag] -> RL (RL (PatchInfoAnd p)) -> RL (PatchInfoAnd p)
matchingHead opts (x:<:_) | or (mapRL (match_a_patchread opts) x) = x
matchingHead opts (x:<:xs) = x +<+ matchingHead opts xs
matchingHead _ NilRL = NilRL

present_participle :: String -> String
present_participle v | last v == 'e' = init v ++ "ing"
                     | otherwise = v ++ "ing"
\end{code}

