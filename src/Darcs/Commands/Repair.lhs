%  Copyright (C) 2003,2005 David Roundy
%
%  This program is free software; you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation; either version 2, or (at your option)
%  any later version.
%
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
%
%  You should have received a copy of the GNU General Public License
%  along with this program; see the file COPYING.  If not, write to
%  the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
%  Boston, MA 02110-1301, USA.

\subsection{darcs repair}
\begin{code}
module Darcs.Commands.Repair ( repair ) where
import System.IO
import System.Exit ( exitWith, ExitCode(..) )

import Darcs.Commands
import Darcs.Arguments ( DarcsFlag(),
                        working_repo_dir, umask_option,
                      )
import Darcs.Repository ( withRepoLock, ($-), amInRepository,
                          replacePristineFromSlurpy )
import Darcs.Repository.Repair( replayRepository, cleanupRepositoryReplay,
                                RepositoryConsistency(..), CanRepair(..) )
\end{code}

\options{repair}
\begin{code}
repair_description :: String
repair_description = "Repair the corrupted repository."
\end{code}
\haskell{repair_help}

\begin{code}
repair_help :: String
repair_help =
 "Repair attempts to fix corruption that may have entered your\n"++
 "repository.\n"
\end{code}

\begin{code}
repair :: DarcsCommand
repair = DarcsCommand {command_name = "repair",
                       command_help = repair_help,
                       command_description = repair_description,
                       command_extra_args = 0,
                       command_extra_arg_help = [],
                       command_command = repair_cmd,
                       command_prereq = amInRepository,
                       command_get_arg_possibilities = return [],
                       command_argdefaults = nodefaults,
                       command_advanced_options = [umask_option],
                       command_basic_options = [working_repo_dir]}
\end{code}

Repair currently will only repair damage to the pristine tree.
Fortunately this is just the sort of corruption that is most
likely to happen.

\begin{code}

repair_cmd :: [DarcsFlag] -> [String] -> IO ()
repair_cmd opts _ = withRepoLock opts $- \repository -> do
  state <- replayRepository CanRepair repository opts
  case state of
      RepositoryConsistent ->
          putStrLn "The repository is already consistent, no changes made."
      RepositoryInconsistent s -> do
               putStrLn "Fixing pristine tree..."
               replacePristineFromSlurpy repository s
  cleanupRepositoryReplay repository
  exitWith ExitSuccess

\end{code}
