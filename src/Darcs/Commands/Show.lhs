%  Copyright (C) 2002-2004 David Roundy
%
%  This program is free software; you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation; either version 2, or (at your option)
%  any later version.
%
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
%
%  You should have received a copy of the GNU General Public License
%  along with this program; see the file COPYING.  If not, write to
%  the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
%  Boston, MA 02110-1301, USA.

\begin{code}
module Darcs.Commands.Show ( show_command, list, query ) where

import Darcs.Commands ( DarcsCommand(..),
                        CommandControl(Command_data, Hidden_command),
                        command_alias,
                      )
import Darcs.Commands.ShowAuthors ( show_authors )
import Darcs.Commands.ShowBug ( show_bug )
import Darcs.Commands.ShowContents ( show_contents )
import Darcs.Commands.ShowFiles ( show_files, show_manifest )
import Darcs.Commands.ShowTags ( show_tags )
import Darcs.Commands.ShowRepo ( show_repo )
import Darcs.Repository ( amInRepository )

show_description :: String
show_description = "Show information which is stored by darcs."

show_help :: String
show_help =
 "Use the --help option with the subcommands to obtain help for\n"++
 "subcommands (for example, \"darcs show files --help\").\n"

show_command :: DarcsCommand
show_command = SuperCommand {command_name = "show",
                      command_help = show_help,
                      command_description = show_description,
                      command_prereq = amInRepository,
                      command_sub_commands = [Hidden_command show_bug,
                                              Command_data show_contents,
                                              Command_data show_files, Hidden_command show_manifest,
                                              Command_data show_repo,
                                              Command_data show_authors,
                                              Command_data show_tags]
                     }

query :: DarcsCommand
query = command_alias "query" show_command

list :: DarcsCommand
list = command_alias "list" show_command
\end{code}

\subsection{darcs show}

The show command provides access to several subcommands which can be
used to investigate the state of a repository.

\input{Darcs/Commands/ShowAuthors.lhs}
\input{Darcs/Commands/ShowContents.lhs}
\input{Darcs/Commands/ShowFiles.lhs}
\input{Darcs/Commands/ShowTags.lhs}
\input{Darcs/Commands/ShowRepo.lhs}

