%  Copyright (C) 2003 David Roundy
%
%  This program is free software; you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation; either version 2, or (at your option)
%  any later version.
%
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
%
%  You should have received a copy of the GNU General Public License
%  along with this program; see the file COPYING.  If not, write to
%  the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
%  Boston, MA 02110-1301, USA.

\begin{code}
module Darcs.TheCommands ( command_control_list ) where

import Darcs.Commands.Add ( add )
import Darcs.Commands.AmendRecord ( amendrecord )
import Darcs.Commands.Annotate ( annotate )
import Darcs.Commands.Apply ( apply )
import Darcs.Commands.Changes ( changes )
import Darcs.Commands.Check ( check )
import Darcs.Commands.Convert ( convert )
import Darcs.Commands.Diff
import Darcs.Commands.Dist ( dist )
import Darcs.Commands.Get ( get )
import Darcs.Commands.Init ( initialize )
import Darcs.Commands.Show ( show_command, list, query )
import Darcs.Commands.MarkConflicts ( markconflicts, resolve )
import Darcs.Commands.Mv ( mv, move )
import Darcs.Commands.Optimize ( optimize )
import Darcs.Commands.Pull ( pull )
import Darcs.Commands.Push ( push )
import Darcs.Commands.Put ( put )
import Darcs.Commands.Record ( record, commit )
import Darcs.Commands.Remove ( remove, rm, unadd )
import Darcs.Commands.Repair ( repair )
import Darcs.Commands.Replace ( replace )
import Darcs.Commands.Revert ( revert )
import Darcs.Commands.Rollback ( rollback )
import Darcs.Commands.Send ( send )
import Darcs.Commands.SetPref ( setpref )
import Darcs.Commands.Tag ( tag )
import Darcs.Commands.TrackDown ( trackdown )
import Darcs.Commands.TransferMode ( transfer_mode )
import Darcs.Commands.Unrecord ( unrecord, unpull, obliterate )
import Darcs.Commands.Unrevert ( unrevert )
import Darcs.Commands.WhatsNew ( whatsnew )
import Darcs.Commands ( CommandControl(Command_data,Hidden_command,Group_name) )

-- | The commands that darcs knows about (e.g. whatsnew, record),
--   organized into thematic groups.  Note that hidden commands
--   are also listed here.
command_control_list :: [CommandControl]
command_control_list = [Group_name "Changing and querying the working copy:",
                Command_data add,
                Command_data remove, Hidden_command unadd, Hidden_command rm,
                Command_data mv, Hidden_command move,
                Command_data replace,
                Command_data revert,
                Command_data unrevert,
                Command_data whatsnew,
                Group_name "Copying changes between the working copy and the repository:",
                Command_data record, Hidden_command commit,
                Command_data unrecord,
                Command_data amendrecord,
                Command_data markconflicts, Hidden_command resolve,
                Group_name "Direct modification of the repository:",
                Command_data tag,
                Command_data setpref,
                Group_name "Querying the repository:",
                Command_data diff_command,
                Command_data changes,
                Command_data annotate,
                Command_data dist,
                Command_data trackdown,
                Command_data show_command, Hidden_command list, Hidden_command query,
                Hidden_command transfer_mode,
                Group_name "Copying patches between repositories with working copy update:",
                Command_data pull,
                Command_data obliterate, Hidden_command unpull,
                Command_data rollback,
                Command_data push,
                Command_data send,
                Command_data apply,
                Command_data get,
                Command_data put,
                Group_name "Administrating repositories:",
                Command_data initialize,
                Command_data optimize,
                Command_data check,
                Command_data repair,
                Command_data convert]

\end{code}
