%  Copyright (C) 2002-2003 David Roundy
%
%  This program is free software; you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation; either version 2, or (at your option)
%  any later version.
%
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
%
%  You should have received a copy of the GNU General Public License
%  along with this program; see the file COPYING.  If not, write to
%  the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
%  Boston, MA 02110-1301, USA.

\chapter{Theory of patches}
\label{Patch}

\newtheorem{thm}{Theorem}
\newtheorem{dfn}{Definition}

\section{Background}

I think a little background on the author is in order.  I am a physicist,
and think like a physicist.  The proofs and theorems given here are what I
would call ``physicist'' proofs and theorems, which is to say that while
the proofs may not be rigorous, they are practical, and the theorems are
intended to give physical insight.  It would be great to have a
mathematician work on this to give patch theory better formalized
foundations.

From the beginning of this theory, which originated as the result of a
series of email discussions with Tom Lord, I have looked at patches as
being analogous to the operators of quantum mechanics.  I include in this
appendix footnotes explaining the theory of patches in terms of the theory
of quantum mechanics.  I advise against taking this analogy too seriously,
although it could provide some insight into how a physicist might think
about darcs.

\begin{code}
{-# OPTIONS_GHC -cpp -fno-warn-orphans #-}
#include "gadts.h"
module Darcs.Patch ( RepoPatch, Prim, Patch, RealPatch, Named, Patchy,
                     flattenFL, joinPatches,
                     fromPrim, fromPrims,
               is_null_patch, nullP,
               rmfile, addfile, rmdir, adddir, move,
               hunk, tokreplace, namepatch, anonymous,
               binary,
               description,
               showContextPatch, showPatch, showNicely,
               infopatch, changepref,
               thing, things,
               is_similar, is_addfile, is_hunk, is_setpref,
#ifndef GADT_WITNESSES
               merger, is_merger, merge,
               commute, commutex, list_touched_files,
               -- for PatchTest
               unravel, elegant_merge,
#else
               Commute(..),
#endif
               resolve_conflicts,
               Effect, effect,
               is_binary, gzWritePatch, writePatch, is_adddir,
               invert, invertFL, invertRL, identity,
               commuteFL, commuteRL,
               readPatch,
               canonize, sort_coalesceFL,
               try_to_shrink,
               apply_to_slurpy, patchname, patchcontents,
               apply_to_filepaths, force_replace_slurpy, apply,
               patch2patchinfo,
               LineMark(AddedLine, RemovedLine, AddedRemovedLine, None),
               MarkedUpFile, markup_file, empty_markedup_file,
               summary, summarize, xml_summary,
               adddeps, getdeps,
               list_conflicted_files,
               modernize_patch,
               -- for Population
               DirMark(..), patchChanges, applyToPop,
             ) where
import Darcs.PopulationData ( DirMark(..) )
import Darcs.Patch.Core ( Patch, Named,
                          flattenFL,
                          adddeps, namepatch,
                          anonymous,
#ifndef GADT_WITNESSES
                          is_merger,
#endif
                          getdeps,
                          is_null_patch, nullP, infopatch,
                          patch2patchinfo, patchname, patchcontents )
import Darcs.Patch.Read ( readPatch )
import Darcs.Patch.Patchy ( Patchy, writePatch, gzWritePatch,
                            showPatch, showNicely, showContextPatch,
                            invert, invertRL, invertFL, identity,
                            thing, things,
                            commuteFL, commuteRL, apply,
                            description, summary,
#ifndef GADT_WITNESSES
                            commute, commutex, list_touched_files,
#else
                            Commute(..)
#endif
                          )
import Darcs.Patch.Viewing ( xml_summary, summarize )
import Darcs.Patch.Apply ( applyToPop, patchChanges, empty_markedup_file,
                           markup_file, force_replace_slurpy,
                           apply_to_filepaths, apply_to_slurpy,
                           LineMark(..), MarkedUpFile )
import Darcs.Patch.Commute ( modernize_patch,
#ifndef GADT_WITNESSES
                             unravel,
                             merger, merge, elegant_merge,
#endif
                            )
import Darcs.Patch.Prim ( FromPrims, fromPrims, joinPatches, FromPrim, fromPrim,
                          Conflict, Effect(effect), list_conflicted_files, resolve_conflicts,
                          Prim, canonize,
                          sort_coalesceFL,
                          rmdir, rmfile, tokreplace, adddir, addfile,
                          binary, changepref, hunk, move, 
                          is_adddir, is_addfile,
                          is_hunk, is_binary, is_setpref,
                          is_similar,
                          try_to_shrink )
import Darcs.Ordered ( FL )
import Darcs.Patch.Real ( RealPatch )

instance Patchy Patch
instance (Conflict p, Effect p, Patchy p) => Patchy (Named p)

class (Patchy p, Effect p, FromPrim p, Conflict p) => RepoPatchBase p
instance RepoPatchBase Patch
instance RepoPatchBase RealPatch

class (Patchy p, Effect p, FromPrims p, Conflict p) => RepoPatch p
instance RepoPatch Patch
instance RepoPatchBase p => RepoPatch (FL p)

\end{code}

\input{Darcs/Patch/Apply.lhs}
\input{Darcs/Patch/Core.lhs}
\input{Darcs/Patch/Commute.lhs}
\input{Darcs/Patch/Show.lhs}

