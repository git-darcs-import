%  Copyright (C) 2004 David Roundy
%
%  This program is free software; you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation; either version 2, or (at your option)
%  any later version.
%
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
%
%  You should have received a copy of the GNU General Public License
%  along with this program; if not, write to the Free Software Foundation,
%  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

\chapter{Configuring darcs}\label{configuring}

There are several ways you can adjust darcs' behavior to suit your needs.
The first is to edit files in the \verb!_darcs/prefs/! directory of a
repository.  Such configuration only applies when working with that
repository.  To configure darcs on a per-user rather than per-repository
basis (but with essentially the same methods), you can edit (or create)
files in the \verb!~/.darcs/! directory (or on Microsoft Windows, the
C:/Documents And Settings/user/Application Data/darcs directory).
Finally, the behavior of some darcs commands can be modified by setting
appropriate environment variables.

\input{Darcs/Repository/Prefs.lhs}

\input{Darcs/Repository/Motd.lhs}

\section{Environment variables}

There are a few environment variables whose contents affect darcs'
behavior.  Here is a quick list of all the variables and their
documentation in the rest of the manual:

\begin{tabular}{|l|r|}
\hline
\textbf{Variable} & \textbf{Section} \\
\hline
DARCS\_EDITOR, EDITOR, VISUAL & \ref{env:DARCS_EDITOR} \\
DARCS\_PAGER, PAGER &  \ref{env:DARCS_PAGER} \\
HOME & \ref{env:HOME} \\
TERM & \ref{env:TERM} \\
\hline
DARCS\_EMAIL, EMAIL  & \ref{env:DARCS_EMAIL} \\
\hline
DARCS\_APPLY\_FOO & \ref{env:DARCS_X_FOO} \\
DARCS\_GET\_FOO & \ref{env:DARCS_X_FOO} \\
DARCS\_MGET\_FOO & \ref{env:DARCS_X_FOO} \\
DARCS\_MGETMAX & \ref{env:DARCS_MGETMAX} \\
DARCS\_PROXYUSERPWD & \ref{env:DARCS_PROXYUSERPWD} \\
DARCS\_WGET & \ref{env:DARCS_WGET} \\
DARCS\_SSH & \ref{env:DARCS_SSH} \\
DARCS\_SCP & \ref{env:DARCS_SCP} \\
DARCS\_SFTP & \ref{env:DARCS_SFTP} \\
SSH\_PORT & \ref{env:SSH_PORT} \\
\hline
DARCS\_ALTERNATIVE\_COLOR & \ref{env:DARCS_ALWAYS_COLOR}\\
DARCS\_ALWAYS\_COLOR & \ref{env:DARCS_ALWAYS_COLOR}\\
DARCS\_DO\_COLOR\_LINES & \ref{env:DARCS_DO_COLOR_LINES}\\
DARCS\_DONT\_COLOR   & \ref{env:DARCS_ALWAYS_COLOR} \\
DARCS\_DONT\_ESCAPE\_TRAILING\_CR     & \ref{env:DARCS_DONT_ESCAPE_white}\\
DARCS\_DONT\_ESCAPE\_TRAILING\_SPACES & \ref{env:DARCS_DONT_ESCAPE_white} \\
DARCS\_DONT\_ESCAPE\_8BIT & \ref{env:DARCS_DONT_ESCAPE_nonascii}\\
DARCS\_DONT\_ESCAPE\_ANYTHING & \ref{env:DARCS_DONT_ESCAPE_nonascii}\\
DARCS\_DONT\_ESCAPE\_ISPRINT & \ref{env:DARCS_DONT_ESCAPE_nonascii}\\
DARCS\_ESCAPE\_EXTRA & \ref{env:DARCS_DONT_ESCAPE_nonascii}\\
DARCS\_DONT\_ESCAPE\_EXTRA & \ref{env:DARCS_DONT_ESCAPE_nonascii}\\
\hline
\end{tabular}

\section{General-purpose variables}

\paragraph{DARCS\_EDITOR}
\label{env:DARCS_EDITOR}
When pulling up an editor (for example, when adding a long comment in
record), darcs uses the contents of DARCS\_EDITOR if it is defined.  If
not, it tries the contents of VISUAL, and if that isn't defined (or fails
for some reason), it tries EDITOR\@.  If none of those environment variables
are defined, darcs tries \verb!vi!, \verb!emacs!, \verb!emacs -nw! and
\verb!nano! in that order.

\paragraph{DARCS\_PAGER}
\label{env:DARCS_PAGER}
When using a pager for displaying a patch, darcs uses the contents of
DARCS\_PAGER if it is defined.  If not, it tries the contents of PAGER
and then \verb!less!.

\paragraph{DARCS\_TMPDIR}
\label{env:DARCS_TMPDIR}
If the environment variable DARCS\_TMPDIR is defined, darcs will use that
directory for its temporaries.  Otherwise it will use TMPDIR, if that is
defined, and if not that then \verb!/tmp! and if \verb!/tmp! doesn't exist,
it'll put the temporaries in \verb!_darcs!.

This is very helpful, for example, when recording with a test suite that
uses MPI, in which case using \verb!/tmp! to hold the test copy is no good,
as \verb!/tmp! isn't shared over NFS and thus the \verb!mpirun! call will
fail, since the binary isn't present on the compute nodes.

\paragraph{DARCS\_KEEP\_TMPDIR}
\label{env:DARCS_KEEP_TMPDIR}
If the environment variable DARCS\_KEEP\_TMPDIR is defined, darcs will
not remove temprary directories.

This can be useful for darcs debugging.

\paragraph{HOME}
\label{env:HOME}
HOME is used to find the per-user prefs directory, which is located at
\verb!$HOME/.darcs!.

%$ this dollar is a comment to make my emacs leave math mode... (stupid
%  emacs)

\paragraph{TERM}
\label{env:TERM}
If darcs is compiled with libcurses support and support for color output,
it uses the environment variable TERM to decide whether or not color is
supported on the output terminal.

\section{Remote repositories}

\paragraph{SSH\_PORT}
\label{env:SSH_PORT}
When using ssh, if the SSH\_PORT environment variable is defined, darcs will
use that port rather than the default ssh port (which is 22).

\paragraph{DARCS\_SSH}
\label{env:DARCS_SSH}
The DARCS\_SSH environment variable defines the command that darcs will use
when asked to run ssh.  This command is \emph{not} interpreted by a shell,
so you cannot use shell metacharacters, and the first word in the command
must be the name of an executable located in your path.

\paragraph{DARCS\_SCP and DARCS\_SFTP}
\label{env:DARCS_SCP}
\label{env:DARCS_SFTP}
The DARCS\_SCP and DARCS\_SFTP environment variables define the
commands that darcs will use when asked to run scp or sftp.  Darcs uses
scp and sftp to access repositories whose address is of the
form \verb!user@foo.org:foo! or \verb!foo.org:foo!.  Darcs will use
scp to copy single files (e.g.\ repository meta-information), and sftp
to copy multiple files in batches (e.g.\ patches).  These commands are
\emph{not} interpreted by a shell, so you cannot use shell
metacharacters, and the first word in the command must be the name of
an executable located in your path.  By default, \verb!scp! and \verb!sftp!
are used.  When you can use sftp, but not scp (e.g.\ some ISP web sites), it
works to set DARCS\_SCP to `sftp'.  The other way around does not work, i.e.\ 
DARCS\_FTP must reference an sftp program, not scp.

\paragraph{DARCS\_PROXYUSERPWD}
\label{env:DARCS_PROXYUSERPWD}
This environment variable allows DARCS and libcurl to access remote repositories
via a password-protected HTTP proxy. The proxy itself is specified with the standard
environment variable for this purpose, namely 'http\_proxy'. The DARCS\_PROXYUSERPWD
environment variable specifies the proxy username and password. It must be given in 
the form \emph{username:password}.

\paragraph{DARCS\_GET\_FOO, DARCS\_MGET\_FOO and DARCS\_APPLY\_FOO}
\label{env:DARCS_X_FOO}
When trying to access a repository with a URL beginning foo://,
darcs will invoke the program specified by the DARCS\_GET\_FOO
environment variable (if defined) to download each file, and the
command specified by the DARCS\_APPLY\_FOO environment variable (if
defined) when pushing to a foo:// URL.  

This method overrides all other ways of getting \verb!foo://xxx! URLs.

Note that each command should be constructed so that it sends the downloaded
content to STDOUT, and the next argument to it should be the URL\@.  Here are some
examples that should work for DARCS\_GET\_HTTP:

\begin{verbatim}
fetch -q -o -  
curl -s -f
lynx -source 
wget -q -O -
\end{verbatim}

If set, DARCS\_MGET\_FOO
will be used to fetch many files from a single repository simultaneously.
Replace FOO and foo as appropriate to handle other URL schemes.
These commands are \emph{not} interpreted by a shell, so you cannot
use shell metacharacters, and the first word in the command must
be the name of an executable located in your path. The GET command
will be called with a URL for each file.  The MGET command will be
invoked with a number of URLs and is expected to download the files
to the current directory, preserving the file name but not the path.
The APPLY command will be called with a darcs patchfile piped into
its standard input. Example:

\begin{verbatim}
wget -q 
\end{verbatim}

\paragraph{DARCS\_MGETMAX}
\label{env:DARCS_MGETMAX}
When invoking a DARCS\_MGET\_FOO command, darcs will limit the
number of URLs presented to the command to the value of this variable,
if set, or 200.

\paragraph{DARCS\_WGET}
\label{env:DARCS_WGET}
This is a very old option that is only used if libcurl is not compiled
in and one of the DARCS\_GET\_FOO is not used. Using one of those
is recommended instead.

The DARCS\_WGET environment variable defines the command that darcs
will use to fetch all URLs for remote repositories.  The first word in
the command must be the name of an executable located in your path.
Extra arguments can be included as well, such as:

\begin{verbatim}
wget -q 
\end{verbatim}

Darcs will append \verb!-i! to the argument list, which it uses to provide a
list of URLS to download. This allows wget to download multiple patches at the
same time. It's possible to use another command besides \verb!wget! with this
environment variable, but it must support the \verb!-i! option in the same way. 

These commands are \emph{not} interpreted by a shell, so you cannot use shell
meta-characters.


\section{Highlighted output}
\label{env:DARCS_ALWAYS_COLOR}
\label{env:DARCS_DO_COLOR_LINES}
\label{env:DARCS_DONT_ESCAPE_white}

If the terminal understands ANSI color escape sequences,
darcs will highlight certain keywords and delimiters when printing patches.
This can be turned off by setting the environment variable DARCS\_DONT\_COLOR to 1.
If you use a pager that happens to understand ANSI colors, like \verb!less -R!,
darcs can be forced always to highlight the output
by setting DARCS\_ALWAYS\_COLOR to 1.
If you can't see colors you can set DARCS\_ALTERNATIVE\_COLOR to 1,
and darcs will use ANSI codes for bold and reverse video instead of colors.
In addition, there is an extra-colorful mode, which is not enabled by
default, which can be activated with DARCS\_DO\_COLOR\_LINES.

By default darcs will escape (by highlighting if possible) any kind of spaces at the end of lines
when showing patch contents.
If you don't want this you can turn it off by setting
DARCS\_DONT\_ESCAPE\_TRAILING\_SPACES to 1.
A special case exists for only carriage returns:
DARCS\_DONT\_ESCAPE\_TRAILING\_CR.


\section{Character escaping and non-ASCII character encodings}
\label{env:DARCS_DONT_ESCAPE_nonascii}

Darcs needs to escape certain characters when printing patch contents to a terminal.
Characters like \emph{backspace} can otherwise hide patch content from the user,
and other character sequences can even in some cases redirect commands to the shell
if the terminal allows it.

By default darcs will only allow printable 7-bit ASCII characters (including space),
and the two control characters \emph{tab} and \emph{newline}.
(See the last paragraph in this section for a way to tailor this behavior.)
All other octets are printed in quoted form (as \verb!^<control letter>! or
\verb!\!\verb!<hex code>!).

Darcs has some limited support for locales.
If the system's locale is a single-byte character encoding,
like the Latin encodings,
you can set the environment variable DARCS\_DONT\_ESCAPE\_ISPRINT to 1
and darcs will display all the printables in the current system locale
instead of just the ASCII ones.
NOTE: This curently does not work on some architectures if darcs is
compiled with GHC~6.4. Some non-ASCII control characters might be printed
and can possibly spoof the terminal.

For multi-byte character encodings things are less smooth.
UTF-8 will work if you set DARCS\_DONT\_ESCAPE\_8BIT to 1,
but non-printables outside the 7-bit ASCII range are no longer escaped.
E.g., the extra control characters from Latin-1
might leave your terminal at the mercy of the patch contents.
Space characters outside the 7-bit ASCII range are no longer recognized
and will not be properly escaped at line endings.

As a last resort you can set DARCS\_DONT\_ESCAPE\_ANYTHING to 1.
Then everything that doesn't flip code sets should work,
and so will all the bells and whistles in your terminal.
This environment variable can also be handy
if you pipe the output to a pager or external filter
that knows better than darcs how to handle your encoding.
Note that \emph{all} escaping,
including the special escaping of any line ending spaces,
will be turned off by this setting.

There are two environment variables you can set
to explicitly tell darcs to not escape or escape octets.
They are
DARCS\_DONT\_ESCAPE\_EXTRA and DARCS\_ESCAPE\_EXTRA.
Their values should be strings consisting of the verbatim octets in question.
The do-escapes take precedence over the dont-escapes.
Space characters are still escaped at line endings though.
The special environment variable DARCS\_DONT\_ESCAPE\_TRAILING\_CR
turns off escaping of carriage return last on the line (DOS style).
