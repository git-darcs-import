const char *curl_request_url(const char *url,
                             const char *filename,
                             int cache_time);

const char *curl_wait_next_url();

const char *curl_last_url();

void curl_enable_debug();
