-- Copyright (C) 2003 David Roundy
--
-- This program is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; either version 2, or (at your option)
-- any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program; if not, write to the Free Software Foundation,
-- Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

module Main (main) where

import Darcs.Commands
import Darcs.TheCommands
import Time

main :: IO ()
main = man (extract_commands command_control_list)

man :: [DarcsCommand] -> IO ()
man cs = do man_header
            unorganized <- man_organizer cs
            putStrLn ".SH OTHER COMMANDS"
            man_helper unorganized
            man_trailer

man_organizer :: [DarcsCommand] -> IO [DarcsCommand]
man_organizer commands = mo commands
                         [("CREATING REPOSITORIES",
                           ["initialize","get"]),
                          ("MODIFYING REPOSITORY CONTENTS",
                           ["add","remove","mv","replace"]),
                          ("WORKING WITH CHANGES",
                           ["record","pull","push","send","apply"]),
                          ("SEEING WHAT YOU'VE DONE",
                           ["whatsnew","query"])
                         ]
    where mo :: [DarcsCommand] -> [(String,[String])] -> IO [DarcsCommand]
          mo cs [] = return cs
          mo cs ((t,a):xs) =
              do putStrLn $ ".SH " ++ t
                 man_helper $ filter ((`elem` a) . command_name) cs
                 mo (filter (not . (`elem` a) . command_name) cs) xs

man_helper :: [DarcsCommand] -> IO ()
man_helper cmds =
  helper' "" cmds
  where helper' _ [] = return ()
        helper' s (c@DarcsCommand { }:cs) = do
            putStrLn $ ".TP"
            putStrLn $ ".B "++ s ++ " "  ++ command_name c
            putStrLn $ command_help c
            helper' s cs
        helper' s (c@SuperCommand { }:cs) = do
            helper' (command_name c ++ " " ++ s)
                      (extract_commands (command_sub_commands c))
            helper' s cs

man_trailer :: IO ()
man_trailer = do putStrLn ""
                 putStrLn ".SH BUGS"
                 putStrLn "Report bugs by mail to"
                 putStrLn ".B bugs@darcs.net"
                 putStrLn "or via the web site at"
                 putStrLn ".BR http://bugs.darcs.net/ ."
                 putStrLn ""
                 putStrLn ".SH AUTHOR"
                 putStrLn "David Roundy <droundy@abridgegame.org>."

man_header :: IO ()
man_header = do
  putStr ".TH DARCS \"1\" \""
  cl <- getClockTime
  ct <- toCalendarTime cl
  putStr $ show (ctMonth ct) ++ " " ++ show (ctYear ct)
  putStr "\" \"darcs\" \"User Commands\"\n"
  putStr $
   ".SH NAME\n"++
   "darcs \\- an advanced revision control system\n"++
   ".SH SYNOPSIS\n"++
   ".B darcs\n"++
   "\\fICOMMAND \\fR...\n"++
   ".SH DESCRIPTION\n"++
   "\n"++
   "darcs is a nifty revision control tool.  For more a detailed description,\n"++
   "see the html documentation, which should be available at\n"++
   "/usr/share/doc/darcs/manual/index.html.  To easily get more specific help\n"++
   "on each command, you can call `darcs COMMAND --help'.\n"++
   "\n"


