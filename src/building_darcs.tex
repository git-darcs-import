\chapter{Building darcs}

This chapter should walk you through the steps necessary to build darcs for
yourself.  There are in general two ways to build darcs.  One is for
building released versions from tarballs, and the other is to build the
latest and greatest darcs, from the darcs repo itself.

Please let me know if you have any problems building darcs, or don't have
problems described in this chapter and think there's something obsolete
here, so I can keep this page up-to-date.

\section{Prerequisites}
To build darcs you will need to have {\tt ghc}, the Glorious Glasgow
Haskell Compiler. You should have at the very minimum version 6.4.

It is a good idea (but not required) to have software installed that provide darcs
network access. The {\tt libwww-dev}, {\tt libwww-ssl-dev} or {\tt libcurl} packages
newer than than 7.18.0 are recommended because they provide pipelining support speed
up HTTP access. They have to be explicitly chosen with {\tt --with-libwww} or
{\tt --with-curl-pipelining}. Otherwise, darcs will automatically look for one of
libcurl, {\tt curl} or {\tt wget}.  You also might want to have scp
available if you want to grab your repos over ssh\ldots

To use the \verb!diff! command of darcs, a \verb!diff! program supporting
options \verb!-r! (recursive diff) and \verb!-N! (show new files as
differences against an empty file) is required. The \verb!configure!
script will look for \verb!gdiff!, \verb!gnudiff! and \verb!diff! in this
order. You can force the use of another program by setting the \verb!DIFF!
environment variable before running \verb!configure!.

To rebuild the documentation (which should not be necessary since it is
included in html form with the tarballs), you will need to have latex
installed, as well as latex2html if you want to build it in html form.


\section{Building on Mac~OS~X}
To build on Mac~OS~X, you will need the Apple Developer Tools and the ghc
6.4 package installed.

\section{Building on Microsoft Windows}
To build on Microsoft Windows, you will need:

\begin{itemize}
\item \htmladdnormallinkfoot{MinGW}{http://www.mingw.org/} which provides the GCC
  toolchain for win32.
\item \htmladdnormallinkfoot{MSYS}{http://www.mingw.org/msys.shtml} which provides
  a unix build environment for win32.  Be sure to download the separate
  msysDTK, autoconf and automake.
\item \htmladdnormallinkfoot{zlib-1.2.1+}{http://www.gzip.org/zlib/} library
  and headers.
\item \htmladdnormallinkfoot{curl-7.12.2+}{http://curl.haxx.se/} library
  and headers.
\item If building with an SSL enabled curl you will need the OpenSSL
  libraries, unofficial builds are available at\\
  \htmladdnormallink{http://www.slproweb.com/products/Win32OpenSSL.html}{http://www.slproweb.com/products/Win32OpenSSL.html}.
\end{itemize}

Copy the zlib and curl libraries and headers to both GHC and MinGW.  GHC
stores C headers in \verb!<ghc-dir>/gcc-lib/include! and libraries in
\verb!<ghc-dir>/gcc-lib!.  MinGW stores headers in
\verb!<mingw-dir>/include! and libraries in \verb!<mingw-dir>/lib!.

Set PATH to include the \verb!<msys-dir>/bin!, \verb!<mingw-dir>/bin!,
\verb!<curl-dir>!, and a directory containing a pre-built darcs.exe if you
want the build's patch context stored for `\verb!darcs --exact-version!'.

\begin{verbatim}
C:\darcs> cd <darcs-source-dir>
C:\darcs> sh

$ export GHC=/c/<ghc-dir>/bin/ghc.exe
$ autoconf
$ ./configure --target=mingw
$ make
\end{verbatim}

\section{Building from tarball}
If you get darcs from a tarball, the procedure (after unpacking the tarball
itself) is as follows:
\begin{verbatim}
% ./configure
% make
# Optional, but recommended
% make test
% make install
\end{verbatim}

There are options to configure that you may want to check out with
\begin{verbatim}
% ./configure --help
\end{verbatim}

If your header files are installed in a non-standard location, you may need
to define the \verb!CFLAGS! and \verb!CPPFLAGS! environment variables to
include the path to the headers.  e.g. on NetBSD, you may need to run
\begin{verbatim}
% CFLAGS=-I/usr/pkg/include CPPFLAGS=-I/usr/pkg/include ./configure
\end{verbatim}

\section{Building darcs from the repository}
To build the latest darcs from its repository, you will first need a
working copy of Darcs 2. You can get darcs using:
\begin{verbatim}
% darcs get -v http://darcs.net/
\end{verbatim}
and once you have the darcs repository you can bring it up to date with a
\begin{verbatim}
% darcs pull
\end{verbatim}

The repository doesn't hold automatically generated files, which include
the configure script and the HTML documentation, so you need to run
\verb!autoconf! first.

You'll need \verb!autoconf! 2.50 or higher. Some systems have more than one
version of \verb!autoconf! installed. For example, \verb!autoconf! may point to
version 2.13, while \verb!autoconf259!  runs version 2.59.

Also note that \verb!make! is really "GNU make". On some systems, such as
the *BSDs, you may need to type \verb!gmake! instead of make for this to work.

If you want to create readable documentation you'll need to have latex installed.
\begin{verbatim}
% autoconf
% ./configure
% make
% make install
\end{verbatim}

If you want to tweak the configure options, you'll need to run {\tt
  ./configure} yourself after the make, and then run make again.

\section{Submitting patches to darcs}
I know, this doesn't really belong in this chapter, but if you're using the
repository version of darcs it's really easy to submit patches to me using
darcs. In fact, even if you don't know any Haskell, you could submit fixes
or additions to this document (by editing \verb!building_darcs.tex!) based
on your experience building darcs\ldots

To do so, just record your changes (which you made in the darcs repository)
\begin{verbatim}
% darcs record --no-test
\end{verbatim}
making sure to give the patch a nice descriptive name.  The
\verb!--no-test! options keeps darcs from trying to run the unit tests,
which can be rather time-consuming.  Then you can send the patch to the
darcs-devel mailing list by email by
\begin{verbatim}
% darcs send
\end{verbatim}
If you are using darcs 2.0.0 or earlier, please use
\begin{verbatim}
% darcs send -u
\end{verbatim}
instead.
The darcs repository stores the email address to which patches should be
sent by default.  The email address you see is actually my own, but when
darcs notices that you haven't signed the patch with my GPG key, it will
forward the message to darcs-devel.

