-- |
-- Program     : git-darcs-import
-- Copyright   : (c) 2008 Bertram Felgenhauer
-- License     : GPL2
--
-- Maintainer  : Bertram Felgenhauer <int-e@gmx.de>
-- Stability   : experimental
-- Portability : ghc
--
-- darcs to git conversion.
--
--    git-darcs-import src dst
--

module Main (main) where

import System.Environment (getArgs)
import System.Directory (canonicalizePath, doesDirectoryExist, createDirectory)
import System.IO (hFlush, stdout)
import Control.Monad (when, liftM, forM_)
import Control.Monad.Trans (liftIO)
import Data.Maybe (isJust)
import System.Console.GetOpt

import Darcs.Repository (read_repo, withRepositoryDirectory, Repository)
import Darcs.Patch.Core (Named (..))
import Darcs.Patch.Info (make_filename, pi_tag)
import Darcs.Patch.Set (PatchSet)
import Darcs.Ordered (mapRL)
import Darcs.Patch (RepoPatch)
import Darcs.Hopefully (PatchInfoAnd, info, hopefully)
import Git.Base (Git, getVerbose, modifyVerbose, failure)
import Git.Fast (runGit, getInventory, applyAndCommit)
import System.Posix.Signals (installHandler, sigPIPE, Handler (..))
import qualified Data.ByteString.Char8 as BC

options :: [OptDescr (Git ())]
options = [
    Option "v" ["verbose"]
           (NoArg (modifyVerbose succ))
           "Be more verbose. This option can be given multiple times."
    ]

main :: IO ()
main = do
    installHandler sigPIPE (CatchOnce $ putStrLn "\nCaught SIGPIPE!") Nothing
    args <- getArgs
    case getOpt Permute options args of
        (opts, [src, dst], []) -> convert opts src dst
        (opts, [src], []) -> convert opts src "."
        (_, _, []) -> putStrLn usage
        (_, _, es) -> mapM_ print es

usage :: String
usage = usageInfo "git-darcs-import [-v] <source> [<destination>]" options

-- convert a darcs repository to a git repository, or catch up on new patches
convert :: [Git ()] -> FilePath -> FilePath -> IO ()
convert opts src dst0 = do
    dst <- canonicalizePath dst0
    dstEx <- doesDirectoryExist dst
    when (not dstEx) $ createDirectory dst
    withRepositoryDirectory [] src (work0 dst)
  where
    toList :: PatchSet p -> [PatchInfoAnd p]
    toList = concatMap (mapRL id) . mapRL id

    work0 :: RepoPatch p => FilePath -> Repository p -> IO ()
    work0 dst repo = do
        ps <- read_repo repo
        runGit (sequence opts) dst (work (toList ps))
        putStrLn "\ngit-darcs-import done."

    -- the worker for convert
    work :: RepoPatch p => [PatchInfoAnd p] -> Git ()
    work ps = do
        inv <- liftM reverse getInventory
        let inv' = map (BC.unpack . fst) inv
        case match ps inv' of
            Nothing -> failure "Destination is not a prefix of source."
            Just ps' -> forM_ ps' $ \(i, p@(NamedP pip _ _)) -> do
                v <- getVerbose
                liftIO $ do
                    putStr $ "\r" ++ show i ++ ". " ++ make_filename pip
                          ++ if v > 0 then "\n" else " "
                    hFlush stdout
                applyAndCommit p

    -- match the patches from the source repository (ps) with the patches in
    -- the destination (qs). for now we assume that the latter is a prefix of
    -- the first.
    match :: [PatchInfoAnd p] -> [String]
          -> Maybe [(Int, Named p)]
    -- case 1) empty destination
    match ps [] = Just $ reverse (zip [1..] (map hopefully ps))
    -- case 2) q is the last patch applied to the destination
    match ps (q:qs) = match' qs q ps [] 1

    -- find q in ps
    match' :: [String]               -- other patches in destination
           -> String                 -- last patch in destination
           -> [PatchInfoAnd p]       -- patches to check
           -> [(Int, Named p)]       -- tentative work queue
           -> Int                    -- patch number
           -> Maybe [(Int, Named p)] -- maybe result work queue
    match' _  _ [] _  _ = Nothing
    match' qs q (p:ps) xs i
        | patchId p == q && samePatches ps qs = Just xs
        | patchId p == q                      = Nothing
        | otherwise = match' qs q ps ((i, hopefully p) : xs) $! i+1

    patchId :: PatchInfoAnd p -> String
    patchId p = make_filename (info p)

    -- check whether the given lists are the same; we stop the scan after
    -- the first tag for better performance
    samePatches :: [PatchInfoAnd p] -> [String] -> Bool
    samePatches (a:as) (b:bs) = patchId a == b && isTag a || samePatches as bs
    samePatches [] [] = True
    samePatches _  _  = False

    isTag :: PatchInfoAnd p -> Bool
    isTag = isJust . pi_tag . info
