#!/usr/bin/runhaskell
import Distribution.Franchise
import Data.Char ( isDigit )
import Control.Monad ( msum )
import System.Directory ( removeFile, getDirectoryContents, doesFileExist )

true_version = "2.1.0"

configure = do version true_version
               defineAs "PACKAGE_VERSION" (show true_version)
               ghcFlags ["-threaded","-Wall","-O2","-funbox-strict-fields"]
               define "_REENTRANT"

               checkWindows
               define "HAVE_SYSTEM_POSIX"

               checkLib "z" "zlib.h" "gzopen(\"hello\", \"r\")"
                            `catchC` \_ ->
                            do requireModule "Codec.Compression.GZip"
                               define "HAVE_HASKELL_ZLIB"
               checkHTTP

               not `fmap` amLittleEndian >>= replace "@BIGENDIAN@"

               -- These are default values
               replace "@USE_COLOR@" True
               replace "@sysconfdir@" "/etc"
               replace "@datadir@" "/usr/local/share"

               do checkHeader "siginfo.h"
                  putS "Found siginfo.h"
                  define "HAVE_SIGINFO_H"
                `catchC` \_ -> return ()

               replace "@USE_MMAP@" True
               replace "@SENDMAIL@" (Literal "/usr/sbin/sendmail")
               replace "@HAVE_SENDMAIL@" True
               replace "@GHC_SEPARATOR@" '/'

               -- check if we can link with mapi library
               -- WARNING: this check may be broken
               have_mapi <- (do checkLib "mapi32" "windows.h" "MAPISendMail()"
                                return True) `catchC` \_ -> return False
               replace "@HAVE_MAPI@" have_mapi

               replace "@DIFF@" (Literal "diff")

               createFile "src/Autoconf.lhs"

               warnSlowHttp

darcs = do extra_cfiles <- checkWindows
           addTarget context
           build' CanModifyState "src/Context.hs"
           addTarget releaseVersion
           build' CanModifyState "src/ThisVersion.lhs"
           let cfiles = ["src/atomic_create.c","src/fpstring.c",
                         "src/c_compat.c","src/hscurl.c","src/maybe_relink.c",
                         "src/hslibwww.c","src/umask.c","src/Crypt/sha2.c"]
                        ++extra_cfiles
           d <- executable "darcs" "src/darcs.lhs" cfiles

           texDocs <- buildTexDocs cfiles
           return d

main = build options configure darcs

options = [ -- configureFlag defines flags coupled with actions to take
            -- when configuring, if the flag is present.  By contrast,
            -- configureUnlessFlag defines actions to take when configuring
            -- if the flag is *not* present.  When building and no
            -- reconfigure is needed (because Setup.hs hasn't been
            -- modified), these flags are all ignored.
            configureFlag "with-bytestring" "use bytestring package"
                     (do requireModule "Data.ByteString"
                         define "HAVE_BYTESTRING"),
            configureFlag "with-static-libs" "link with static versions of libraries"
                     (do ldFlags ["-static"]
                         addExtraData "STATIC" ""),
            configureUnlessFlag "without-libwww" "do not use libwww" checkLibWWW,
            configureUnlessFlag "without-libcurl" "do not use libcurl" checkCurl,
            configureFlag "with-type-witnesses" "for gadt type witnesses"
                     (do define "GADT_WITNESSES"
                         ghcFlags ["-fglasgow-exts"]),
            configureUnlessFlag "without-haskeline" "do not use haskeline, even if it is present" $
                  withModuleExporting "System.Console.Haskeline"
                         "runInputT, defaultSettings, getInputLine, handleDyn"
                         "runInputT defaultSettings (getInputLine \"prompt: \") :: IO (Maybe String)"
                         $ define "HAVE_HASKELINE",
            configureUnlessFlag "without-curses" "don't use libcurses" $
                       withLib "curses" "term.h" "tgetnum(\"foobar\")" $
                       define "HAVE_CURSES",
            -- flag and unlessFlag describe flags that have effect even
            -- when we are not configuring, which may override the
            -- configured value.
            flag "disable-color" "do not use ansi color escapes"
                     (replace "@USE_COLOR@" False),
            unlessFlag "disable-Werror" "don't fail on warnings" $ ghcFlags ["-Werror"]
          ]

context = (["src/Context.hs"] :< ["_darcs/hashed_inventory"])
              |<- defaultRule { make = const makeContext }
  where makeContext =
            do c <- systemOut "darcs" ["changes","--context"]
                    `catchC` \_ -> return "unknown context"
               io $ writeFile "src/Context.hs" "module Context where\n"
               io $ appendFile "src/Context.hs" "context :: String\n"
               io $ appendFile "src/Context.hs" ("context = "++show c++"\n")
               io $ appendFile "src/Context.hs" "\n"

data Literal = Literal String
instance Show Literal where
    showsPrec _ (Literal x) = showString x

releaseVersion =
    ["src/ThisVersion.lhs","doc/index.html"]
    :< ["_darcs/hashed_inventory", "src/ThisVersion.lhs.in",
        "doc/index.html.in", extraData "version"]
        |<- defaultRule { make = const findReleaseVersion }
  where findReleaseVersion =
            do patches' <- systemOut "darcs"
                           ["changes","--from-tag",true_version,"--count"]
                           `catchC` \_ -> return "-1"
               ((patches'',_):_) <- return $ reads patches' ++ [(-1,"")]
               let patches = if patches'' > 0 then patches'' - 1 else patches''
                   Just state =
                       if patches == 0
                       then msum [niceVersion "pre" "prerelease" true_version,
                                  niceVersion "rc" "release candidate" true_version,
                                  isRelease true_version,
                                  Just "tag"]
                       else if patches > 1
                            then Just $ "+ "++show patches++" patches"
                            else if patches < 0
                                 then Just "unknown"
                                 else Just $ "+ "++show patches++" patch"
               xxx <- systemOut "darcs" ["changes","-t","'^[0-9\\.]+\\$", "--reverse"]
                      `catchC` \_ -> return ""
               let lastrelease = case map words $ lines xxx of
                                 ((_:zzz:_):_) -> zzz
                                 _ -> "unknown"
               replace "VERSION" true_version
               replace "RELEASE" lastrelease
               replace "STATE" state
               replace "@DARCS_VERSION_STATE@" (Literal state)
               replace "@DARCS_VERSION@" (Literal true_version)
               createFile "doc/index.html"
               createFile "src/ThisVersion.lhs"
               -- Various extra files we generate...
               createFile "darcs.cabal"
               createFile "release/darcs.spec"
               createFile "tools/cgi/darcs.cgi"
               createFile "tools/cgi/README"
               createFile "tools/cgi/cgi.conf"

        niceVersion _ _ "" = Nothing
        niceVersion x n y
            | take (length x) y == x &&
              all isDigit (drop (length x) y) = Just $ n++" "++drop (length x) y
        niceVersion "" _ _ = Nothing
        niceVersion x n (_:y) = niceVersion x n y
        isRelease x = if all (\c -> isDigit c || c == '.') x
                      then Just "release"
                      else Nothing

checkLibWWW = unlessC (haveExtraData "HAVE_LIBCURL") $
              unlessC (haveExtraData "HAVE_LIBWWW") $ -- avoid running this twice!
              do io $ putStr "Checking for libwww... "
                 do systemOut "libwww-config" ["--version"]
                    define "HAVE_LIBWWW"
                    lds <- systemOut "libwww-config" ["--libs"]
                    ldFlags $ words lds
                    cfs <- systemOut "libwww-config" ["--cflags"]
                    cFlags $ words cfs
                    define "HAVE_LIBWWW"
                    cFlags ["-DHAVE_LIBWWW"]
                    addExtraData "HAVE_LIBWWW" ""
                    putS "found."
                  `catchC` \_ -> putS "not found."

checkCurl = unlessC (haveExtraData "HAVE_LIBWWW") $
            unlessC (haveExtraData "HAVE_LIBCURL") $ -- avoid running this twice!
            do io $ putStr "Checking for libcurl... "
               curlconfig <-findProgram "curl-config" []
               ver <- systemOut curlconfig ["--version"]
               amstatic <- haveExtraData "STATIC"
               lds <- if amstatic
                      then do define "CURL_STATICLIB"
                              systemOut curlconfig ["--static-libs"]
                      else systemOut curlconfig ["--libs"]
               ldFlags $ words lds
               cfs <- systemOut curlconfig ["--cflags"]
               cFlags $ words cfs
               define "HAVE_CURL"
               putS $ "yes, "++filter (/= '\n') ver
               io $ putStr "Checking whether libcurl actually works... "
               checkLib "curl" "curl/curl.h" "curl_global_init(0)"
               -- check whether we've got curl_multi_timeout...
               do checkLib "curl" "curl/curl.h" "curl_multi_timeout(1,2)"
                  define "CURL_MULTI_TIMEOUT"
                `catchC` \_ -> return ()
               vernumstr <- systemOut curlconfig ["--vernum"]
               let vernum = read ("0x0"++vernumstr) :: Int
                   minver  = 0x071200 -- 7.18.0 has working buggy pipelining
                   goodver = 0x071301 -- 7.19.1 has bug-free pipelining
               if vernum < minver
                  then putS "Curl pipelining requires libcurl version >= 7.18.0"
                  else do define "CURL_PIPELINING"
                          if vernum < goodver
                            then do putS "Curl pipelining in this version is buggy with http proxies."
                                    putS "WARNING:  Not enabling http pipelining by default!"
                            else do define "CURL_PIPELINING_DEFAULT"
               addExtraData "HAVE_LIBCURL" ""
            `catchC` \_ -> putS "libcurl not found"

checkHTTP = do havehttp <- lookForModule "Network.HTTP"
               if havehttp
                  then do define "HAVE_HTTP"
                          replace "@HAVE_HTTP@" True
                  else do putS "Network.HTTP not found"
                          replace "@HAVE_HTTP@" False

warnSlowHttp =
    do havefast <- or `fmap` mapM isDefined ["HAVE_LIBWWW", "HAVE_LIBCURL"]
       havehttp <- isDefined "HAVE_HTTP"
       case (havefast, havehttp) of
         (True,_) -> return ()
         (_,True) -> do putS "WARNING: http access will be slow."
                        putS "  To get faster http, install libcurl or libwww."
         (_,_) -> do x <- findProgram "wget" ["curl"]
                     putS $ "WARNING: http access will use the external program "++x
                     putS "  This will be very slow.  For faster http, install libcurl, "
                     putS "  libwww or the http Haskell package."
                 `catchC` \_ ->
                  do putS "WARNING:  There is no way for darcs to access files over http!"
                     putS "  To allow darcs to access files over http, please install"
                     putS "  either libcurl, libwww, the http Haskell package, or wget"
                     putS "  or curl."


pureTexSources = ["src/features.tex", "src/switching.tex",
                  "src/configuring_darcs.tex", "src/gpl.tex",
                  "src/building_darcs.tex", "src/best_practices.tex",
                  "src/formats.tex"]

buildTexDocs cfiles =
    do findProgram "pdflatex" []
       preproc <- privateExecutable "preproc" "src/preproc.hs" cfiles
       addTarget $ ["src/darcs_print.pdf"] :< ["src/darcs_print.tex"] |<-
                 defaultRule { make = \_ -> do cd "src"
                                               system "pdflatex" ["darcs_print"] }
       addTarget $ ["src/darcs_print.tex"] :< (preproc:pureTexSources) |<-
                   defaultRule { make = \_ -> do x <- systemOut "./preproc" ["darcs.lhs"]
                                                 io $ writeFile "src/darcs_print.tex" x }
       return ["src/darcs_print.pdf"]
   `catchC` \_ -> do putS "WARNING: Couldn't find pdflatex, can't build docs"
                     return []


checkWindows = do amwin <- amInWindows
                  if amwin then do putV "apparently we're on windows..."
                                   replace "@USE_MMAP@" False
                                   define "WIN32"
                                   ghcFlags ["-isrc/win32","-Isrc/win32"]
                                   systemOut "hsc2hs" ["src/win32/System/Posix/Files.hsc"]
                                   systemOut "hsc2hs" ["src/win32/System/Posix/IO.hsc"]
                                   return ["src/win32/CtrlC_stub.o"]
                           else do putV "apparently we're not on windows..."
                                   return []
