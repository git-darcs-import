#!/bin/env bash

set -ev

## The builtin ! has the wrong semantics for not.
not () { "$@" && exit 1 || :; }

rm -rf temp
mkdir temp
cd temp

not darcs show bug &> bugout
cat bugout
grep 'lease report this' bugout

cd ..
rm -rf temp
