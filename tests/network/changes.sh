#!/usr/bin/env bash
set -ev

# Demonstrates issue385

test "$DARCS" || DARCS="$PWD"/../darcs

"$DARCS" changes --repo=http://darcs.net GNUmakefile --last 300
