#!/usr/bin/env bash
set -ev

not () { "$@" && exit 1 || :; }

# issue70 and RT #349 - setpref should coalesce changes

rm -rf temp1
mkdir temp1
cd temp1
darcs init
darcs setpref predist apple
darcs setpref predist banana
darcs setpref predist clementine
darcs record -a  -m manamana
darcs changes --verbose > log
not grep apple log
not grep banana log
grep clementine log
cd ..
rm -rf temp1

# not sure what i'm going for here - if coalescing happens strictly
# before commuting, no problem, but what if patches are commuted 
# before coalescing?
mkdir temp1
cd temp1
darcs init
darcs setpref predist apple
darcs setpref predist banana
darcs setpref predist apple
darcs setpref predist clementine
darcs setpref predist banana
darcs record -a  -m manamana
darcs changes --verbose > log
not grep apple log
not grep clementine log
grep banana log
cd ..
rm -rf temp1
