#!/usr/bin/env bash
set -ev

not () { "$@" && exit 1 || :; }

rm -rf temp1 temp2
mkdir temp1
cd temp1
darcs  init
echo gobbledygook > _darcs/format
cd ..

not darcs get temp1 temp2 2> log
grep -i "can't understand repository format" log
rm -rf temp1 temp2 log
