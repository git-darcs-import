#!/usr/bin/env bash
set -ev
rm -rf test
mkdir test
cd test
darcs --version
darcs init
echo test >File.hs
darcs add File.hs
darcs record File.hs -a -m "add File"
rm File.hs
darcs record -a -m "rm File"
darcs cha
darcs unrecord -p "rm File" -a
darcs cha
darcs record -a -m "re-rm File"
