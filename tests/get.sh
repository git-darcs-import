#!/usr/bin/env bash
set -ev

# darcs does not support cygwin paths (/cygdrive/c/foo), so if
# we want to run the test suite under Cygwin Bash, we must
# convert accordingly
portable_pwd () {
  if uname | grep -i cygwin > /dev/null; then
    cygpath -w `pwd`
  else
    pwd
  fi
}

rm -rf first
mkdir first
cd first
darcs init
touch t.t
darcs add t.t
darcs record -am "initial add"
darcs changes --context > my_context
IFS='' # annoying hack for cygwin and portable_pwd below
DIR=`portable_pwd`
abs_to_context=${DIR}/my_context
cd ..
darcs get first --context=${abs_to_context} second
rm -rf first
