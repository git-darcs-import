#!/usr/bin/env bash

# http://bugs.darcs.net/issue1057: Identifying current repository when pulling from repository identified via symbolic link

set -ev

## I would use the builtin !, but that has the wrong semantics.
not () { "$@" && exit 1 || :; }

# darcs does not support cygwin paths (/cygdrive/c/foo), so if
# we want to run the test suite under Cygwin Bash, we must
# convert accordingly
portable_pwd () {
  if uname | grep -i cygwin > /dev/null; then
    cygpath -w `pwd`
  else
    pwd
  fi
}

rm -rf temp
mkdir temp
cd temp

mkdir repo
cd repo
darcs init
cd ..

ln -s repo srepo
cd srepo
IFS='' # annoying hack for cygwin and portable_pwd below
DIR=`portable_pwd`
echo $DIR
not darcs pull --debug -a "$DIR" 2> out
cat out
grep 'Can.t pull from current repository' out
cd ..

cd ..
rm -rf temp
