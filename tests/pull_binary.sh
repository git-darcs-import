#!/usr/bin/env bash

# This test script, originally written by David Roundy and Ian Lynagh is in
# the public domain.

set -ev

rm -rf temp1 temp2

mkdir temp1
cd temp1
darcs init
perl -e 'print "a"x1048576' > foo
darcs record -l -a -A author -m xx
rm foo
darcs record -a -A author -m yy
cd ..

mkdir temp2
cd temp2
darcs init
echo yn | darcs pull ../temp1
rm foo
darcs pull -a
cd ..

rm -rf temp1 temp2
