#!/usr/bin/env bash

set -ev

# Check that the right patches get sent using --dont-prompt-for-dependencies

rm -rf temp1
rm -rf temp2
mkdir temp2
mkdir temp1
cd temp2
darcs init
cd ..
cd temp1
darcs init
echo foo > f
darcs record -Ax -alm foo1
echo bar > b
darcs rec -Ax -alm bar1
echo foo2 > f
darcs record -Ax -alm foo2
echo bar2 > b
darcs record -Ax -alm bar2
echo y | darcs send ../temp2 --dont-prompt-for-dependencies -p foo2 --dry-run > toto
#on the previous line, we don't get asked about foo1.
grep foo toto | wc -l | grep 2
#but we send it anyway.
grep bar toto | wc -l | grep 0
cd ..
rm -rf temp1 temp2
