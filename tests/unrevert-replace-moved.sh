#!/usr/bin/env bash
set -ev

## The builtin ! has the wrong semantics for not.
not () { "$@" && exit 1 || :; }

rm -rf temp1
mkdir temp1
cd temp1
darcs init
echo hello world > foo
darcs add foo

darcs record -a -m 'addfoo'

darcs replace hello goodbye foo

darcs revert -a

not darcs whatsnew

darcs mv foo bar

echo hello my good friends >> bar

darcs unrevert -a

darcs whatsnew > unrecorded
cat unrecorded

grep 'bar .* hello goodbye' unrecorded

cat bar
grep 'goodbye world' bar
grep 'goodbye my good friends' bar

cd ..
rm -rf temp1
