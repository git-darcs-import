#!/usr/bin/env bash

# A partial get of a repo shall have the same _darcs/pristine as the original

set -ev

rm -rf temp
mkdir temp
cd temp

# Create a development repo, do some work

darcs initialize --darcs-2
echo ALL ignore-times >> _darcs/prefs/defaults
touch a
darcs add a
darcs record -a -m aa -A x
touch b
darcs add b
darcs record -a -m bb -A x

# Create a release repo, pull the good patches and tag it

mkdir _rel
cd _rel
darcs initialize --darcs-2 
darcs pull -a --patch a ..
darcs tag -m tt -A x
cd ..

# Pull the tag to the devel repo and continue developement

darcs pull -a _rel
touch c
darcs add c
darcs record -a -m cc -A x

# Create a checkpoint and get a partial temp repo

darcs tag -A x --checkpoint first_checkpoint 
darcs get --partial . _partial

# We should have all our devel files

cd _partial
cat a
cat b
cat c

# This is a regression test for issue406
darcs tag -A x --checkpoint checkpointing_a_partial

cd ..

# With the darcs-2 format, doing a get on a partial repo succeeds.
# With the darcs-1 format it would be required to add the --partial flag, or darcs would give an error. 
darcs get _partial _second_partial

cd ..
rm -rf temp
