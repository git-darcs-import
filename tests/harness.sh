#!/usr/bin/env bash

set -ev

# Print some stuff out for debugging if something goes wrong:
echo $HOME
echo $PATH
which darcs
command -v darcs

# Check things that should be true when all the testscripts run

test -f "$HOME"/harness.sh || { echo "HOME=\"$HOME\" is not the test suite directory"; exit 1; }

homedirname=`dirname "$HOME"`
command -v darcs | grep "$homedirname"
