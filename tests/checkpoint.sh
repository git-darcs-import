#!/usr/bin/env bash

# A test for unrecording checkpoint tags, inspired by issue517

set -ev

darcs --version

rm -rf temp temp1 temp2
mkdir temp

# This the last commands would blow up if the "--darcs-2" format wasn't used.
darcs init --darcs-2 --repodir temp
touch temp/x
darcs rec -lam xx --repodir temp
darcs tag -m yy --repodir temp
touch temp/z
darcs rec -lam zz --repodir temp
darcs optimize --checkpoint --repodir temp
darcs get --partial temp temp1
echo 'y' |  darcs unrec -p TAG --repodir temp1

# Now, the real tests. Do they return sensible results? 
darcs get --partial temp1 temp2;
echo 'n' | darcs unrec --repodir temp1 -p 'zz'

rm -rf temp temp1 temp2;
darcs init --darcs-2 --repodir temp
touch temp/x
darcs rec -lam xx --repodir temp
darcs tag -m yy --repodir temp
touch temp/z
darcs rec -lam zz --repodir temp
darcs optimize --checkpoint --repodir temp
darcs get --partial temp temp1
echo 'y' |  darcs unrec -p TAG --repodir temp1

# Now, the real tests. Do they return sensible results? 
darcs get --partial temp1 temp2;
echo 'n' | darcs unrec --repodir temp1 -p 'zz'

rm -rf temp temp1 temp2;
