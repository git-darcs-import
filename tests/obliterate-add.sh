#!/usr/bin/env bash
set -ev

## The builtin ! has the wrong semantics for not.
not () { "$@" && exit 1 || :; }

rm -rf temp1
mkdir temp1
cd temp1
darcs init
echo foo > foo
darcs add foo

darcs record -a -m 'addfoo'

darcs obliterate -a

not darcs whatsnew

cd ..
rm -rf temp1
