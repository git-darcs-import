#!/usr/bin/env bash
set -ev

# trick: requiring something to fail
not () { "$@" && exit 1 || :; }

# trick: portably getting the pwd
portable_pwd () {
  if uname | grep -i cygwin > /dev/null; then
    cygpath -w `pwd`
  else
    pwd
  fi
}
# setting IFS is an ugly hack for Cygwin
# so that the portable_pwd backtick
IFS=''
DIR=`portable_pwd`

# trick: OS-detection (if needed)
if echo $OS | grep -i windows; then
  echo This test does not work on Windows
  exit 0
fi

# set up the repository
rm -rf temp1                    # another script may have left a mess.
mkdir temp1
cd temp1
darcs init
touch foo
darcs add foo
# trick: hitting 'enter' in interactive darcs record
echo n/ | tr / \\012 | darcs record
cd ..
rm -rf temp1
