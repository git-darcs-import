#!/usr/bin/env bash

set -ev

# Check that checkpoints are removed when tags are unrecorded

rm -rf temp1
mkdir temp1
cd temp1
darcs init
echo foo > f
darcs rec -Ax -alm init
darcs tag -Ax --checkpoint t
echo y | darcs unrec -t t
# the complete inventory has only one patch to check
darcs check --complete > out
# The following test fails because the new progress code only
# prints out information on slow commands.
#grep '1 of 1' out >/dev/null

# the checkpoint (if left) has two patches to check
# but it should have been removed
darcs check --partial > out
# The following test fails because the new progress code only
# prints out information on slow commands.
#grep '1 of 1' out >/dev/null
cd ..
rm -rf temp1
