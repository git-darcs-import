#!/usr/bin/env bash
set -ev

# A test for pulling a patch to remove a directory when the local copy is not empty.
# Issue154

not () { "$@" && exit 1 || :; }

rm -rf temp1
mkdir temp1
cd temp1
darcs init
mkdir d
darcs add d
darcs record -a -m "Added directory d"
darcs get ./ puller
cd puller
touch d/moo
darcs add d/moo
cd ..
rm -rf d
darcs record -a -m "Remove directory d"
cd puller
echo y | darcs pull -a .. > log
grep -i "backing up" log
grep -i "finished pulling" log
cd ..
rm -rf temp1
