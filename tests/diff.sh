#!/usr/bin/env bash
set -ev

rm -rf temp-$$
mkdir temp-$$
cd temp-$$
set -e
darcs initialize
echo text > afile.txt
darcs add afile.txt
darcs record --author me --all --no-test --patch-name init
darcs diff
darcs diff -p . --store > diffinmem
darcs diff -p . > diffondisk
diff diffinmem diffondisk
cd ..

rm -rf temp-$$
