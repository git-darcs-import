#!/usr/bin/env bash
set -ev

## The builtin ! has the wrong semantics for not.
not () { "$@" && exit 1 || :; }

rm -rf temp1
mkdir temp1
cd temp1
darcs init
echo foo > foo
darcs add foo
darcs whatsnew > correct
cat correct

darcs revert -a

not darcs whatsnew

darcs unrevert -a

darcs whatsnew > unrecorded
cat unrecorded

diff -u correct unrecorded

cd ..
rm -rf temp1
