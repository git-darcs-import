#!/usr/bin/env bash
set -ev

# Some tests for 'darcs printer (the output formating)'

not () { "$@" && exit 1 || :; }

rm -rf temp1
mkdir temp1
cd temp1
darcs init
touch a
darcs add a
darcs rec -a -m add

env
# clear all output formating environment variables
for e in DARCS_DONT_ESCAPE_ISPRINT DARCS_USE_ISPRINT\
         DARCS_DONT_ESCAPE_8BIT\
         DARCS_DONT_ESCAPE_EXTRA DARCS_ESCAPE_EXTRA\
         DARCS_DONT_ESCAPE_TRAILING_SPACES\
         DARCS_DONT_COLOR DARCS_ALWAYS_COLOR DARCS_ALTERNATIVE_COLOR\
         DARCS_DONT_ESCAPE_ANYTHING; do
    unset $e
done
env

# make sure the locale is c
export LC_ALL=C

test_line () {
    rm -f a
    echo $1 > a
    darcs whatsnew | fgrep $2
}


# First check escaping and coloring.  Use whatsnew, since that is the
# most common use of escapes.

# test the defaults
# - no color to pipe
# - don't escape 7-bit ASCII printables, \n,\t and space (we can't test \n)
# - escape control chars with ^
# - escape other chars with \xXX
test_line " !#%&',-0123456789:;<=>"\
          " !#%&',-0123456789:;<=>"
test_line "ABCDEFGHIJKLMNOPQRSTUVWXYZ_"\
          "ABCDEFGHIJKLMNOPQRSTUVWXYZ_"
test_line "\`abcdefghijklmnopqrstuvwxyz"\
          "\`abcdefghijklmnopqrstuvwxyz"
test_line "\t\"\$()*+./?\@[\\]^{|}"\
          "\t\"\$()*+./?\@[\\]^{|}"
# skip ^@ and ^Z since they make darcs treat the file as binary
# don't put any space control chars at end of line
# ascii control chars are escaped with ^
test_line $(printf '\x01\x02\x03\x04\x05\x06\x07\x08\x0B\x0C\x0D\x0E')\
          '[_^A_][_^B_][_^C_][_^D_][_^E_][_^F_][_^G_][_^H_][_^K_][_^L_][_^M_][_^N_]'
test_line $(printf '\x0F\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19')\
          '[_^O_][_^P_][_^Q_][_^R_][_^S_][_^T_][_^U_][_^V_][_^W_][_^X_][_^Y_]'
test_line $(printf '\x1B') '[_^[_]'
test_line $(printf '\x1C') '[_^\_]'
test_line $(printf '\x1D') '[_^]_]'
test_line $(printf '\x1E') '[_^^_]'
test_line $(printf '\x1F') '[_^__]'
test_line $(printf '\x7F') '[_^?_]'
# other chars are escaped with \xXX
test_line $(printf '\x80\x81\x82\x83\x84\x85\x86\x87\x88\x89\x8A\x8B\x8C\x8D\x8E\x8F')\
          '[_\80_][_\81_][_\82_][_\83_][_\84_][_\85_][_\86_][_\87_][_\88_][_\89_][_\8a_][_\8b_][_\8c_][_\8d_][_\8e_][_\8f_]'
test_line $(printf '\x90\x91\x92\x93\x94\x95\x96\x97\x98\x99\x9A\x9B\x9C\x9D\x9E\x9F')\
          '[_\90_][_\91_][_\92_][_\93_][_\94_][_\95_][_\96_][_\97_][_\98_][_\99_][_\9a_][_\9b_][_\9c_][_\9d_][_\9e_][_\9f_]'
test_line $(printf '\xA0\xA1\xA2\xA3\xA4\xA5\xA6\xA7\xA8\xA9\xAA\xAB\xAC\xAD\xAE\xAF')\
          '[_\a0_][_\a1_][_\a2_][_\a3_][_\a4_][_\a5_][_\a6_][_\a7_][_\a8_][_\a9_][_\aa_][_\ab_][_\ac_][_\ad_][_\ae_][_\af_]'
test_line $(printf '\xB0\xB1\xB2\xB3\xB4\xB5\xB6\xB7\xB8\xB9\xBA\xBB\xBC\xBD\xBE\xBF')\
          '[_\b0_][_\b1_][_\b2_][_\b3_][_\b4_][_\b5_][_\b6_][_\b7_][_\b8_][_\b9_][_\ba_][_\bb_][_\bc_][_\bd_][_\be_][_\bf_]'
test_line $(printf '\xC0\xC1\xC2\xC3\xC4\xC5\xC6\xC7\xC8\xC9\xCA\xCB\xCC\xCD\xCE\xCF')\
          '[_\c0_][_\c1_][_\c2_][_\c3_][_\c4_][_\c5_][_\c6_][_\c7_][_\c8_][_\c9_][_\ca_][_\cb_][_\cc_][_\cd_][_\ce_][_\cf_]'
test_line $(printf '\xD0\xD1\xD2\xD3\xD4\xD5\xD6\xD7\xD8\xD9\xDA\xDB\xDC\xDD\xDE\xDF')\
          '[_\d0_][_\d1_][_\d2_][_\d3_][_\d4_][_\d5_][_\d6_][_\d7_][_\d8_][_\d9_][_\da_][_\db_][_\dc_][_\dd_][_\de_][_\df_]'
test_line $(printf '\xE0\xE1\xE2\xE3\xE4\xE5\xE6\xE7\xE8\xE9\xEA\xEB\xEC\xED\xEE\xEF')\
          '[_\e0_][_\e1_][_\e2_][_\e3_][_\e4_][_\e5_][_\e6_][_\e7_][_\e8_][_\e9_][_\ea_][_\eb_][_\ec_][_\ed_][_\ee_][_\ef_]'
test_line $(printf '\xF0\xF1\xF2\xF3\xF4\xF5\xF6\xF7\xF8\xF9\xFA\xFB\xFC\xFD\xFE\xFF')\
          '[_\f0_][_\f1_][_\f2_][_\f3_][_\f4_][_\f5_][_\f6_][_\f7_][_\f8_][_\f9_][_\fa_][_\fb_][_\fc_][_\fd_][_\fe_][_\ff_]'
rm -rf temp1
