#!/usr/bin/env bash
### http://bugs.darcs.net/issue496
### _darcs/prefs/defaults ignored when using --repodir
set -ev

## All these commands SHOULD fail (hence leading NOTs).
## I would use the builtin !, but that has the wrong semantics.
not () { "$@" && exit 1 || :; }

rm -rf temp
mkdir temp
cd temp

mkdir repo
darcs initialize --repodir repo
cd repo
date > foo
darcs add foo
darcs record -a -m auth

echo > _darcs/prefs/defaults ALL disable # try to disable all
not darcs changes
not darcs changes --repodir "$PWD"

cd ..

not darcs changes --repodir repo
not darcs changes --repodir "$PWD/repo"

rm -rf temp
