#!/usr/bin/env bash

## The builtin ! has the wrong semantics for not.
not () { "$@" && exit 1 || :; }
# This test script is in the public domain.

set -ev

rm -rf temp1 temp2 temp3

mkdir temp1
cd temp1
darcs initialize
echo A > A
darcs add A
darcs record -a -m Aismyname
echo B > B
darcs add B
darcs record -a -m Bismyname

cd ..
darcs get temp1 temp2

cd temp2
darcs obliterate --last 1 -a
echo C > C
darcs add C
darcs record -a -m Cismyname
cd ..

mkdir temp3
cd temp3
darcs init
darcs pull -a -v --intersection ../temp1 ../temp2

darcs changes > out
cat out
grep Aismyname out
not grep Bismyname out
not grep Cismyname out
cd ..

rm -rf temp1 temp2 temp3
