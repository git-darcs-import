#!/usr/bin/env bash
set -ev

rm -rf temp
mkdir temp
cd temp
darcs init
echo 'record patch-name' > _darcs/prefs/defaults # patch-name requires an argument
echo 'ALL unified foobar' >> _darcs/prefs/defaults # unified takes no argument
darcs record && exit 1
darcs whats && exit 1
rm -rf temp
