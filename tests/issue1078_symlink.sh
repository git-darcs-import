#!/usr/bin/env bash
set -ev

# darcs does not support cygwin paths (/cygdrive/c/foo), so if
# we want to run the test suite under Cygwin Bash, we must
# convert accordingly
portable_pwd () {
  if uname | grep -i cygwin > /dev/null; then
    cygpath -w `pwd`
  else
    pwd
  fi
}


if echo $OS | grep -i windows; then
    echo this test does not work on windows because 
    echo windows does not have symlinks
    exit 0
fi

rm -rf temp1 temp2
mkdir temp1
ln -s temp1 temp2
cd temp2
darcs init
touch a b
IFS='' # annoying hack for cygwin and portable_pwd below
DIR=`portable_pwd`
darcs add ${DIR}/../temp1/a # should work, just to contrast with the case below
darcs add ${DIR}/b          # this is the case we are testing for
cd ..
rm -rf temp1 temp2
