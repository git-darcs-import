#!/usr/bin/env bash
set -ev

not () { "$@" && exit 1 || :; }
alias pwd=hspwd

rm -rf temp1 temp2
mkdir temp1
cd temp1
darcs init
cd ..

rm -rf temp1 temp2
