#!/usr/bin/env bash
set -ev

# Some tests for 'darcs rec --edit-long-comment'

not () { "$@" && exit 1 || :; }

# trick: OS-detection (if needed)
if echo $OS | grep -i windows; then
  echo This test does not work on Windows
  exit 0
fi

rm -rf temp1

export DARCS_EDITOR="/bin/cat -n"
# editor: space in command
mkdir temp1
cd temp1
darcs init
touch file.t
darcs add file.t
darcs record --edit-long-comment -a -m foo file.t | grep '2.*END OF DESCRIPTION'
cd ..
rm -rf temp1

# editor: space in path
mkdir temp2\ dir
cd temp2\ dir
darcs init
touch file.t
darcs add file.t
darcs record --edit-long-comment -a -m foo file.t | grep '2.*END OF DESCRIPTION'
cd ..
rm -rf temp2\ dir

export DARCS_EDITOR='grep "END OF"'
# editor: quoting in command
mkdir temp1
cd temp1
darcs init
touch file.t
darcs add file.t
darcs record --edit-long-comment -a -m foo file.t | grep 'END OF'
cd ..
rm -rf temp1

export DARCS_EDITOR='/bin/echo'
# editor: evil filename
mkdir temp1
cd temp1
darcs init
touch file.t
darcs add file.t
touch '; test-command'
echo > test-command << FOO
#!/bin/sh
echo EVIL
FOO
chmod u+x test-command
darcs record --logfile='; test-command' --edit-long-comment -a -m foo file.t > log
not grep EVIL log
cd ..
rm -rf temp1
