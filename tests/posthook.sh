#!/usr/bin/env bash

set -ev

rm -rf temp1
mkdir temp1
cd temp1
darcs init
touch foo
darcs add foo

# Check that posthook works...
darcs whatsnew -s --posthook 'touch posthook-ran'
test -f posthook-ran
rm posthook-ran

# Check that posthook works with defaults...
echo ALL --posthook touch posthook-ran > _darcs/prefs/defaults
darcs whatsnew -s
test -f posthook-ran
rm posthook-ran

echo Successful.

cd ..
rm -rf temp1
