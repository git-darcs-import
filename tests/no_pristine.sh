#!/usr/bin/env bash

# This test script, originally written by David Roundy and Ian Lynagh is in
# the public domain.

set -ev

rm -rf temp1

mkdir temp1
cd temp1

darcs initialize --no-pristine-tree
echo ALL --author Testing Person >> _darcs/prefs/defaults
echo ALL --ignore-times >> _darcs/prefs/defaults

echo foo > bar
darcs add bar
darcs record -a -m baz bar

date > bar
darcs record --no-test -a -m 'patch 2'

darcs setpref test 'echo hello world'
darcs record -a -m 'setpref'

date >> bar
darcs record -a -m 'record with test'

cd ..

rm -rf temp1
