#!/usr/bin/env bash

# test for broken time stamps

set -ev

not () { "$@" && exit 1 || :; }

rm -rf temp
mkdir temp
cd temp
darcs init --hashed

echo this is my favorite test > foobar
darcs add foobar
sleep 2 # so the time stamps won't accidentally be identical
darcs record -am 'add foobar'

HASHVAL=0000000025-4cbbfd8ce543076b132b13b60ae06d0189ee80b4d5908abb5e060d331d25eb5c

if test -d _darcs/pristine.hashed;
then
  ls _darcs/pristine.hashed
  test -f _darcs/pristine.hashed/$HASHVAL
  # verify that the modification time of the file in the pristine cache
  # is identical to the modification time of the file in the working
  # directory.
  not test foobar -ot _darcs/pristine.hashed/$HASHVAL
  not test foobar -nt _darcs/pristine.hashed/$HASHVAL
else
  not test foobar -ot _darcs/pristine/foobar
  not test foobar -nt _darcs/pristine/foobar
fi

# Now let's verify that the test actually works...

sleep 1
touch foobar

if test -d _darcs/pristine.hashed;
then
  not test foobar -ot _darcs/pristine.hashed/$HASHVAL
  test foobar -nt _darcs/pristine.hashed/$HASHVAL
else
  not test foobar -ot _darcs/pristine/foobar
  test foobar -nt _darcs/pristine/foobar
fi


cd ..

rm -rf temp
