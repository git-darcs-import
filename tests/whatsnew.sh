#!/usr/bin/env bash
set -ev

not () { "$@" && exit 1 || :; }

portable_pwd () {
  if uname | grep -i cygwin > /dev/null; then
    cygpath -w `pwd`
  else
    pwd
  fi
}

# Some tests for 'darcs whatsnew '

rm -rf temp1 temp2

mkdir temp1
cd temp1

# RT#505 whatsnew -s after removal of file without a newline
darcs init
echo -n foobar > foo
darcs record -la -m "add foo" | grep "Finished record"
rm -f foo
darcs whatsnew -s | grep R
darcs record -a -m "remove foo"

# RT#245 --look-for-adds implies --summary
touch look_summary.txt
darcs whatsnew -l | grep -i "a ./look_summary.txt"

#whatsnew works with uncommon file names
if echo $OS | grep -i windows; then
  echo  test does not work on windows
  exit 0;
else
  touch \\
  darcs add \\
  darcs whatsnew > log
  not grep "no changes" log
fi

# whatsnew works with absolute paths
IFS='' # annoying hack for cygwin and portable_pwd below
DIR=`portable_pwd`
echo date.t > date.t
touch date.t
darcs add date.t
darcs whatsnew ${DIR}/date.t | grep hunk

cd ..

rm -rf temp1
