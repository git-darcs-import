#!/usr/bin/env bash
set -ev

not () { "$@" && exit 1 || :; }

rm -rf temp1
mkdir temp1
cd temp1
darcs init
touch foo
darcs add foo
darcs record -a -m add_foo | grep -i "finished recording"
darcs dist
cd ..

rm -rf temp1
