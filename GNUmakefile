#
# Build darcs
#
ifeq (,$(findstring disttest,$(MAKECMDGOALS)))
ifeq (,$(findstring slowdisttest,$(MAKECMDGOALS)))
ifeq (,$(findstring clean,$(MAKECMDGOALS)))
ifneq (config,$(MAKECMDGOALS))
include autoconf.mk
endif
endif
endif
endif

ifneq (,$(RTSFLAGS))
GHCFLAGS += +RTS $(RTSFLAGS) -RTS
endif

GHCFLAGS += -Wall -Werror -I. -I./src -i./src
GHCFLAGS_src__Darcs__Commands__Record.o = $(filter-out -auto-all,$(GHCFLAGS))
GHCFLAGS_src__Darcs__SelectChanges.o = $(filter-out -auto-all,$(GHCFLAGS))
GHCFLAGS_src__SHA1.o = $(GHCFLAGS) -no-auto-all -funfolding-use-threshold20
ifeq ($(HAVE_CURSES),True)
GHCFLAGS_src__Darcs__External.o = $(GHCFLAGS)
ifeq ($(HAVE_TERMIO_H),True)
# This termio bit is a grim hack for Solaris 10 (at least).
# Without it, SGTTY isn't declared.  Something in the ghc C headers
# which get included before term.h in the generated C is clobbering
# it.  (If you edit that C and put the curses.h and term.h first, the
# error goes away.)
GHCFLAGS_src__Darcs__External.o += '-\#include <termio.h>'
endif
GHCFLAGS_src__Darcs__External.o += '-\#include <curses.h>' '-\#include <term.h>'
endif

SRC_DIRS := . $(shell find src/ -name sys -prune -o -type d -print)

MODULES_AUTOCONF :=\
	Autoconf.lhs \
	Context.hs \

MODULES_GENERAL :=\
	ThisVersion.lhs \
	CommandLine.lhs \
	URL.hs HTTP.hs DateMatcher.lhs \
	English.lhs Exec.lhs\
	ByteStringUtils.hs UglyFileName.lhs IsoDate.lhs \
	Lcs.lhs OldDate.lhs \
	Printer.lhs	\
	RegChars.lhs \
	SHA1.lhs Ssh.hs UTF8.lhs	\
	Workaround.hs \

MODULES_CRYPT := SHA256.hs

MODULES_DARCS :=\
	ArgumentDefaults.lhs \
	Arguments.lhs Bug.lhs \
	CheckFileSystem.lhs \
	ColorPrinter.lhs 	\
	Commands.lhs CommandsAux.lhs RunCommand.lhs \
	Compat.hs \
	Diff.lhs Email.hs FilePathUtils.hs \
	FilePathMonad.lhs	\
	External.hs	\
	Flags.lhs Global.lhs 	\
	Hopefully.lhs \
	IO.lhs Lock.lhs Ordered.lhs Patch.lhs	\
	Match.lhs \
	Population.hs PopulationData.hs	\
	PrintPatch.lhs Progress.lhs \
	RemoteApply.lhs	RepoPath.hs \
	Repository.lhs Resolution.lhs SignalHandler.lhs Test.lhs URL.hs \
	Sealed.lhs Show.lhs \
	SlurpDirectory.lhs \
	SelectChanges.lhs TheCommands.lhs	\
	Utils.lhs		\

MODULES_PATCH:=\
	Apply.lhs Bundle.lhs	\
	Commute.lhs Core.lhs	\
	Choices.lhs \
	Depends.lhs FileName.lhs \
	Info.lhs Match.lhs MatchData.lhs	\
	Non.lhs \
	Patchy.lhs Permutations.lhs \
	Prim.lhs \
	Read.lhs ReadMonads.hs Real.lhs Set.lhs Show.lhs 	\
	TouchesFiles.lhs Viewing.lhs	\

MODULES_REPOSITORY:=\
	ApplyPatches.lhs \
	Cache.lhs \
	Checkpoint.lhs \
	HashedIO.lhs HashedRepo.lhs \
	Format.lhs Internal.lhs Repair.lhs \
	InternalTypes.lhs \
	Motd.lhs Prefs.lhs		\
	Pristine.lhs \
	DarcsRepo.lhs	\

MODULES_COMMANDS:=\
	Add.lhs AmendRecord.lhs Annotate.lhs Apply.lhs		\
	Changes.lhs Check.lhs Convert.lhs Diff.lhs Dist.lhs	\
	Get.lhs	Help.lhs Init.lhs MarkConflicts.lhs Mv.lhs Optimize.lhs 		\
	Pull.lhs Push.lhs Put.lhs	\
	Record.lhs Remove.lhs Repair.lhs Replace.lhs	\
	Revert.lhs Rollback.lhs	\
	Send.lhs SetPref.lhs \
	Show.lhs ShowAuthors.lhs ShowBug.lhs \
	ShowContents.lhs ShowFiles.lhs ShowTags.lhs ShowRepo.lhs	\
	Tag.lhs TrackDown.lhs TransferMode.lhs	\
	Unrecord.lhs Unrevert.lhs WhatsNew.lhs \

witnesses: src/Darcs/Patch/Real.hi src/Darcs/Patch/Properties.hi src/Darcs/Patch.hi \
	src/Darcs/Repository/ApplyPatches.hi src/Darcs/Patch/Bundle.hi \
	src/Darcs/Patch/Match.hi src/Darcs/Repository/HashedRepo.hi \
	src/Darcs/Resolution.hi src/Darcs/Patch/Check.hi \
	src/Darcs/Repository/Pristine.hi src/Darcs/Repository/DarcsRepo.hi \
	src/Darcs/Repository/Internal.hi \
	src/Darcs/Commands/Unrevert.hi src/Darcs/Commands/WhatsNew.hi \
	src/Darcs/Commands/Dist.hi src/Darcs/Commands/TransferMode.hi

DARCS_FILES := \
	$(patsubst %,src/%,$(MODULES_AUTOCONF))\
	$(patsubst %,src/%,$(MODULES_GENERAL))\
	$(patsubst %,src/Crypt/%,$(MODULES_CRYPT))\
	$(patsubst %,src/Darcs/%,$(MODULES_DARCS))\
	$(patsubst %,src/Darcs/Patch/%,$(MODULES_PATCH))\
	$(patsubst %,src/Darcs/Repository/%,$(MODULES_REPOSITORY))\
	$(patsubst %,src/Darcs/Commands/%,$(MODULES_COMMANDS))

UNIT_FILES := $(DARCS_FILES) src/Darcs/Patch/Test.lhs src/Darcs/Patch/QuickCheck.lhs \
	 src/Darcs/Patch/Unit.lhs src/Darcs/Patch/Properties.lhs src/Darcs/Patch/Check.lhs

C_OBJS := $(patsubst %,src/%.o,c_compat maybe_relink atomic_create fpstring umask Crypt/sha2 hscurl hslibwww)

ifeq ($(SYS), windows)
GHCFLAGS += -isrc/win32 -Isrc/win32 -DWIN32
DARCS_FILES += src/win32/System/Posix.hs src/win32/System/Posix/Files.hs \
               src/win32/System/Posix/IO.hs src/win32/CtrlC.hs
C_OBJS += src/win32/CtrlC_stub.o src/win32/CtrlC.o
UNIT_FILES += src/win32/System/Posix.hs src/win32/System/Posix/Files.hs \
              src/win32/System/Posix/IO.hs
endif

ifeq ($(HAVE_MAPI),True)
C_OBJS += src/win32/send_email.o
CPPFLAGS += -Isrc/win32
GHCFLAGS += -DHAVE_MAPI
endif

DARCS_OBJS	:= $(patsubst %.hs,%.o,$(patsubst %.lhs,%.o,$(DARCS_FILES)))
UNIT_OBJS	:= $(patsubst %.hs,%.o,$(patsubst %.lhs,%.o,$(UNIT_FILES)))


.PHONY:		all clean distclean maintainer-clean dist deb test check install \
		test-franchise test-old test-hashed test-format2 \
		darcs-nightly\
		disttest slowdisttest \
		bugs bugs-old bugs-hashed bugs-format2 \
		installserver installdocs installbin windowsinstaller \
		website witnesses\
		api-doc-dir

config: configure autoconf.mk

copy:
	$(MAKE) install prefix=$(destdir)/$(prefix) \
                        bindir=$(destdir)/$(bindir) \
                        libdir=$(destdir)/$(libdir) \
                        datadir=$(destdir)/$(datadir) \
                        libexecdir=$(destdir)/$(libexecdir)

installbin:	all
	test -d $(DESTDIR)$(bindir) || $(INSTALL) -d $(DESTDIR)$(bindir)
	$(INSTALL) darcs $(DESTDIR)$(bindir)

	test -d $(DESTDIR)$(sysconfdir)/bash_completion.d || \
		$(INSTALL) -d $(DESTDIR)$(sysconfdir)/bash_completion.d
	$(INSTALL_DATA) tools/darcs_completion $(DESTDIR)$(sysconfdir)/bash_completion.d/darcs

	test -d $(DESTDIR)$(mandir)/man1 || \
		$(INSTALL) -d $(DESTDIR)$(mandir)/man1
	$(INSTALL_DATA) doc/darcs.1 $(DESTDIR)$(mandir)/man1/

installdocs:	doc/manual/darcs.pdf doc/manual/patch-theory.pdf doc/manual/index.html
	test -d $(DESTDIR)$(docdir)/manual || \
		$(INSTALL) -d $(DESTDIR)$(docdir)/manual
	$(INSTALL_DATA) doc/manual/*.pdf $(DESTDIR)$(docdir)/manual
	$(INSTALL_DATA) doc/manual/*.png $(DESTDIR)$(docdir)/manual
	$(INSTALL_DATA) doc/manual/*.html $(DESTDIR)$(docdir)/manual

	test -d $(DESTDIR)$(docdir)/examples || \
		$(INSTALL) -d $(DESTDIR)$(docdir)/examples
	$(INSTALL_DATA) tools/zsh_completion_new $(DESTDIR)$(docdir)/examples/
	$(INSTALL_DATA) tools/zsh_completion_old $(DESTDIR)$(docdir)/examples/

installserver:	tools/cgi/darcs.cgi
	test -d $(DESTDIR)$(libexecdir)/cgi-bin || \
		$(INSTALL) -d $(DESTDIR)$(libexecdir)/cgi-bin

	test -d $(DESTDIR)$(sysconfdir)/darcs || \
		$(INSTALL) -d $(DESTDIR)$(sysconfdir)/darcs
	test -e $(DESTDIR)$(sysconfdir)/darcs/cgi.conf || \
	    $(INSTALL_DATA) tools/cgi/cgi.conf $(DESTDIR)$(sysconfdir)/darcs/cgi.conf
	$(INSTALL) tools/cgi/darcs.cgi $(DESTDIR)$(libexecdir)/cgi-bin/darcs.cgi
	test -d $(DESTDIR)$(datadir)/darcs || \
		$(INSTALL) -d $(DESTDIR)$(datadir)/darcs
	test -d $(DESTDIR)$(datadir)/darcs/xslt || \
		$(INSTALL) -d $(DESTDIR)$(datadir)/darcs/xslt
	$(INSTALL_DATA) tools/cgi/xslt/*.xslt $(DESTDIR)$(datadir)/darcs/xslt/
	$(INSTALL_DATA) tools/cgi/xslt/errors.xml $(DESTDIR)$(datadir)/darcs/xslt/
	$(INSTALL_DATA) tools/cgi/xslt/styles.css $(DESTDIR)$(datadir)/darcs/xslt/styles.css

# Debian policy doesn't allow symlinks as configuration files.
#	test -e $(DESTDIR)$(sysconfdir)/darcs/styles.css || \
#	    ln -s $(datadir)/darcs/xslt/styles.css \
#		$(DESTDIR)$(sysconfdir)/darcs/styles.css
	$(INSTALL) -m 644 tools/cgi/xslt/styles.css $(DESTDIR)$(sysconfdir)/darcs/styles.css

PREDIST_COPY_FILES := \
	AUTHORS \
	release/STATE \
	src/Context.hs

DARCS_VERSION_WITH_PATCHES=$(shell sed -e 's/ (//' -e 's/ patches)//' -e 's/+ /+/' release/STATE)
darcs-nightly: darcs release/STATE
	ln -sf darcs darcs-$(DARCS_VERSION_WITH_PATCHES)

dist: darcs $(PREDIST_COPY_FILES)
	REPO_PATH="$$PWD" ./darcs dist --dist-name darcs-$(DARCS_VERSION)
	ln -sf darcs-$(DARCS_VERSION).tar.gz darcs-latest.tar.gz

darcs-test.tar.gz: darcs $(PREDIST_COPY_FILES)
	REPO_PATH="$$PWD" ./darcs dist --dist-name darcs-test

predist_copy:
	for F in $(PREDIST_COPY_FILES); do \
	    cp "$$REPO_PATH/$$F" "$$F"; \
	done

predist: predist_copy doc/manual/darcs.ps doc/darcs.1 doc/manual/index.html distclean
	rm -fr release/debian

#ensure the directory exists for api documentation
api-doc-dir:
	@echo "Creating api-doc directory"
	@mkdir -p api-doc

#generate the api documentation
api-doc: api-doc-dir $(DARCS_FILES)
	@test $(HADDOCK_VERSION) -lt 2 || { echo "You need haddock 2.0.0 or later to build the API documentation"; false; }
	echo "Generating html"
	haddock --ignore-all-exports --optghc=-I. --optghc=-I./src --optghc=-i./src -B `$(GHC) --print-libdir` -o api-doc -h $(filter-out %api-doc-dir,$^)
	cp api-doc/index.html api-doc/modules-index.html

#generate the api documentation with links to colourised source code
api-doc-with-source: api-doc-dir colourised-source $(DARCS_FILES) src/darcs.lhs
	@test $(HADDOCK_VERSION) -lt 2 || { echo "You need haddock 2.0.0 or later to build the API documentation"; false; }
	echo "Generating haddock api docs"
	haddock --no-warnings --ignore-all-exports --optghc=-I. --optghc=-I./src --optghc=-i./src -B `$(GHC) --print-libdir` -o api-doc -h --source-module=src-%{MODULE/./-}.html $(filter-out %api-doc-dir colourised-source,$^)
	cp api-doc/index.html api-doc/modules-index.html
# --source-entity=src-%{MODULE/./-}.html#%N # awaiting http://code.google.com/p/ndmitchell/issues/detail?id=95

# munge haddock and hoogle into a rough but useful framed layout
# ensure that the hoogle cgi is built with base target "main"
api-doc-frames: api-doc-with-source hoogleweb
	echo "Converting api docs to frames"
	sed -i -e 's%^></head%><base target="main"></head%i' api-doc/modules-index.html
	cp doc/misc/api-doc-frames.html api-doc/index.html
	cp doc/misc/hoogle-small.html hoogle

colourised-source: api-doc-dir $(DARCS_FILES) src/darcs.lhs
	echo "Generating colourised source"
	for f in $(DARCS_FILES) src/darcs.lhs; do \
		HsColour -css -anchor $$f -oapi-doc/`echo $$f | sed -e's%/%-%g' | sed -e's%\.l\?hs$$%.html%'` ; \
	done
	mv api-doc/src-darcs.html api-doc/src-Main.html
	HsColour -print-css >api-doc/hscolour.css

#generate a hoogle index
#uses system "hoogle"
#works around http://code.google.com/p/ndmitchell/issues/detail?id=93
#usage: hoogle --data=api-doc/default ...
hoogleindex: $(DARCS_FILES) src/darcs.lhs
	echo "Generating hoogle index"
	mkdir -p hoogle
	haddock --no-warnings --ignore-all-exports --optghc=-I. --optghc=-I./src --optghc=-i./src -B `$(GHC) --print-libdir` -o hoogle --hoogle $^
	cd hoogle && sed -i -e 's/^(_/-- (_/' main.txt
	cd hoogle && hoogle --convert=main.txt --output=default.hoo

#set up the hoogle web interface
#uses a hoogle source tree configured with --datadir=. and patched to fix haddock urls
HOOGLESRC=/usr/local/src/hoogle
HOOGLE=$(HOOGLESRC)/dist/build/hoogle/hoogle
HOOGLEVER=`$(HOOGLE) --version |tail -n 1 | sed -e 's/Version /hoogle-/'`
hoogleweb: hoogleindex
	echo "Configuring hoogle web interface"
	mkdir -p hoogle
	ln -sf . hoogle/$(HOOGLEVER)
	cp -r $(HOOGLESRC)/deploy/res/ hoogle/
	cp -p $(HOOGLE) hoogle/index.cgi
	touch log.txt
	chmod 666 log.txt

windowsinstaller:	darcs doc/manual/index.html release/darcs.nsi
	echo "!define VERSION $(DARCS_VERSION)" > version.nsi
	"$(MAKENSIS)" version.nsi release/darcs.nsi
	rm -f version.nsi

darcs:		$(DARCS_OBJS) src/darcs.lhs $(C_OBJS)
	@echo Linking $@ ...
	@rm -f Main.o Main.hi
ifdef VERBOSE
	$(GHC) $(GHCFLAGS) $(OPTLLDFLAGS) -o $@ $^ $(OPTLLIBS)
else
	@$(GHC) $(GHCFLAGS) $(OPTLLDFLAGS) -o $@ $^ $(OPTLLIBS)
endif

# rebuild whenever a module changes, using searchpath
# Requires a patched version: darcs get http://joyful.com/repos/searchpath
SP=sp --no-default-map --no-exts --sp-verbose 1
SPDARCSCMD="--version"
SPLAST=--run $(SPDARCSCMD)
continuous ci:
	@$(SP) $(GHC) --make $(GHCFLAGS) $(OPTLLDFLAGS) -o darcs src/darcs.lhs $(C_OBJS) $(OPTLLIBS) $(SPLAST)

ghci: $(DARCS_FILES) $(C_OBJS)
	ghci $(filter-out -Wall,$(GHCFLAGS)) $(OPTLLDFLAGS) src/darcs.lhs $(C_OBJS) $(OPTLLIBS)
.PHONY: ghci

darcsman:		src/darcsman.hs $(C_OBJS) $(DARCS_OBJS)
list_authors:	src/list_authors.hs $(C_OBJS) $(DARCS_OBJS)
microbench:	src/microbench.hs $(C_OBJS) $(DARCS_OBJS)
unit:			src/unit.lhs $(UNIT_OBJS) $(C_OBJS)
preproc:		src/preproc.hs $(C_OBJS) $(DARCS_OBJS)
git-darcs-import: src/git-darcs-import.hs $(C_OBJS) $(DARCS_FILES) src/Git/*.hs
	$(GHC) -o $@ $(GHCFLAGS) --make  $(OPTLLDFLAGS) $< $(OPTLLIBS) $(C_OBJS) -optl-s

TEXSOURCES := preproc src/darcs.lhs src/features.tex \
	src/switching.tex src/configuring_darcs.tex src/gpl.tex \
	$(DARCS_FILES) src/building_darcs.tex \
	src/best_practices.tex src/formats.tex
src/darcs.tex: $(TEXSOURCES)
	./preproc darcs.lhs $(PREPROCHTML) >$@
# The following is to prevent both being generated simultaneously, which
# confuses texlive:
src/darcs_print.pdf: src/darcs_print.ps
src/darcs_print.tex: $(TEXSOURCES)
	./preproc darcs.lhs >$@
src/patch-theory.tex: $(TEXSOURCES) $(UNIT_FILES)
	./preproc Darcs/Patch/Properties.lhs >$@

website: doc/manual/darcs.ps doc/manual/patch-theory.pdf doc/manual/index.html doc/manual/bigpage.html \
	darcs doc/index.html

doc/manual/index.html: src/darcs.tex src/gpl.tex doc/darcs.css
	$(MAKEMANUAL)
	cp -f doc/darcs.css doc/manual/darcs.css

doc/manual/bigpage.html: src/darcs.tex src/gpl.tex doc/darcs.css
	ln -sf darcs.tex src/bigpage.tex
	latex2html -split 0 -external_file darcs -prefix big \
		-no_auto_link -dir doc/manual src/bigpage.tex
	rm src/bigpage.tex
	cp -f doc/darcs.css doc/manual/bigpage.css

doc/manual/darcs.ps:	src/darcs_print.ps
	cp src/darcs_print.ps doc/manual/darcs.ps
doc/manual/darcs.pdf:	src/darcs_print.pdf
	cp src/darcs_print.pdf doc/manual/darcs.pdf
doc/manual/patch-theory.pdf: src/patch-theory.pdf
	cp src/patch-theory.pdf doc/manual/

doc/darcs.1:	darcsman
	./darcsman > $@

AUTHORS:	list_authors
	./list_authors >$@

check test: test-old test-hashed test-format2

disttest:
	@echo '**********************************************************************'
	@echo Note: to prevent darcs from running tests by default, you can put
	@echo the following entries in _darcs/prefs/defaults
	@echo
	@echo record no-test
	@echo amend-record no-test
	@echo
	@echo It would be helpful if you ran the tests sometime before sending in
	@echo your patches.  You can do so by running darcs check.
	@echo
	@echo To run a faster subset of the tests, just run make check.
	@echo '**********************************************************************'
	@if ghc-pkg list QuickCheck | grep '\-2\..' > /dev/null ; then true; else \
	  echo "You need QuickCheck 2 to run the tests ( darcs get http://darcs.haskell.org/QuickCheck )";\
	  exit 1;\
	fi
	@echo '*****************************'
	@echo Get a clean copy of franchise
	@echo '*****************************'
	rm -rf franchise-test Distribution
	darcs get http://darcs.net/repos/franchise franchise-test
	cp -r franchise-test/Distribution .
	@echo '*****************************'
	@echo Test franchise build of darcs
	@echo '*****************************'
	runghc Setup.hs configure
	runghc Setup.hs build -j4
	runghc Setup.hs clean
	$(call echo_without_wine,'**************************************')
	$(call echo_without_wine,Not testing with older version of wine)
	$(call echo_without_wine,'**************************************')
	$(call echo_with_wine,'****************************************')
	$(call echo_with_wine,Test franchise build of darcs under wine)
	$(call echo_with_wine,'****************************************')
	$(call with_wine,wine runghc Setup.hs configure)
	$(call with_wine,wine runghc Setup.hs build -j4)
	$(call with_wine,wine runghc Setup.hs clean)
	@echo '**************************'
	@echo Running old autoconf tests
	@echo '**************************'
	autoconf
	./configure --with-type-witnesses
	$(MAKE) -j4 witnesses
	$(MAKE) clean
	./configure
	$(MAKE) -j4 all unit
	$(MAKE) -j4 check test_network

slowdisttest: disttest test_unit

bugs:	bugs-old bugs-hashed bugs-format2 \

TEST_FILTER_FILE := tests_to_run
SHELL_HARNESS := perl ../tests/shell_harness

define with_wine
	@if test "wine-1.0" = `wine --version`; then echo "$1"; $1; fi
endef

define echo_with_wine
	@if test "wine-1.0" = `wine --version`; then echo "$1"; fi
endef

define echo_without_wine
	@if test !"wine-1.0" = `wine --version`; then echo "$1"; fi
endef

# $1 - description
# $2 - setup
define shell_harness
	@echo Running tests on $1 repositories...
	@chmod -R u+rwx tests-$1.dir 2>/dev/null || true
	@rm -rf tests-$1.dir && cp -R tests tests-$1.dir
	@cd tests-$1.dir && rm -rf .darcs; mkdir .darcs;\
	 $2;\
	 if [ -r $(TEST_FILTER_FILE) ];\
	 then grep "\.sh" $(TEST_FILTER_FILE) | grep -v '^#' | xargs $(SHELL_HARNESS); \
	 else printf %s\\n *.sh | sort -r | xargs $(SHELL_HARNESS); fi
	@echo Done running tests on $1 repositories...
endef

define bug_harness
	@echo Demonstrating bugs on $1 repositories...
	@rm -rf bugs-$1.dir && cp -R bugs bugs-$1.dir
	@cd bugs-$1.dir && rm -rf .darcs; mkdir .darcs;\
	 $2;\
	 if [ -r $(TEST_FILTER_FILE) ];\
	 then bugs=1 grep "\.sh" $(TEST_FILTER_FILE) | grep -v '^#' | xargs $(SHELL_HARNESS) || true; \
	 else bugs=1 printf %s\\n *.sh | sort -r | xargs $(SHELL_HARNESS) || true; fi
	@echo Done demonstrating bugs on $1 repositories...
endef

test_network: darcs
	@echo Running network tests ...
	@rm -rf tests-network.dir && cp -R tests/network tests-network.dir
	@cd tests-network.dir && rm -rf .darcs; mkdir .darcs;\
	 if [ -r $(TEST_FILTER_FILE) ];\
	 then bugs=1 grep "\.sh" $(TEST_FILTER_FILE) | grep -v '^#' | xargs $(SHELL_HARNESS); \
	 else bugs=1 ls *.sh | sort -r | xargs $(SHELL_HARNESS); fi
	@echo Passed network tests.

hspwd: tests/hspwd.hs
	ghc --make -o hspwd tests/hspwd.hs

test-old:     darcs hspwd
	$(call shell_harness,old-fashioned,\
	  echo ALL --old-fashioned-inventory >> .darcs/defaults)

# for the format2 tests, we skip the hashed_inventory.sh test, since explicitly
# creates the old-fashioned format that's incompatible with darcs-2.
test-format2:     darcs hspwd
	$(call shell_harness,darcs-2,\
	 echo ALL --darcs-2 >> .darcs/defaults)

test-hashed:     darcs hspwd
	$(call shell_harness,hashed,\
	 echo ALL --hashed >> .darcs/defaults)

bugs-old:     darcs hspwd
	$(call shell_bugs,old-fashioned,\
	  echo ALL --old-fashioned-inventory >> .darcs/defaults)

bugs-format2:     darcs hspwd
	$(call shell_bugs,format-2,\
	 echo ALL --darcs-2 >> .darcs/defaults)

bugs-hashed:     darcs hspwd
	$(call shell_bugs,hashed,\
	 echo ALL --hashed >> .darcs/defaults)

test_unit:     darcs unit
	./unit

register:

clean:
	rm -f $(foreach dir,$(SRC_DIRS),$(dir)/*.o $(dir)/*.hi)
	rm -f unit diff darcs darcs_cgi
	rm -f stringify
	rm -f src/darcs.tex src/darcs.dvi src/darcs.aux
	rm -f src/patch-theory.tex src/patch-theory.aux src/patch-theory.log
	rm -f src/patch-theory.aux src/patch-theory.toc
	rm -f src/darcs_print.tex src/darcs_print.log src/darcs_print.aux src/darcs_print.toc
	rm -f preproc darcsman
	rm -rf tests/temp* darcs.log darcs.toc
	rm -f conftest* .depend.bak
	rm -f src/win32/System/Posix/IO.hs src/win32/System/Posix/Files.hs
	rm -f version.nsi
	rm -f list_authors
	rm -rf api-doc
	rm -rf hoogle
	#only remove src/Context.hs if we have a darcs inventory to rebuild it from
	if test -f _darcs/inventory; then rm -f src/Context.hs; fi
	rm -f TAGS tags
	rm -f git-darcs-import

distclean:	clean
	rm -rf config.status config.log autoconf.mk config.cache autom4te.cache
	rm -f doc/manual/darcs.ps doc/manual/darcs.pdf doc/manual/patch-theory.pdf src/Autoconf.lhs darcs.idv
	rm -f src/ThisVersion.lhs tools/cgi/README tools/cgi/darcs.cgi tools/cgi/cgi.conf
	rm -f config.command

maintainer-clean: distclean
	@echo 'This command is intended for maintainers to use; it'
	@echo 'deletes files that may need special tools to rebuild.'
	rm -f configure doc/darcs.1 .depend AUTHORS
	rm -f src/Context.hs
	rm -f doc/manual/*.html doc/manual/darcs*.??? doc/manual/darcs.lg
	rm -f doc/manual/darcs.xref c_context.c doc/darcs_print.ps
	rm -rf tests/test_output

configure:		configure.ac aclocal.m4
	autoconf

autoconf.mk src/Autoconf.lhs tools/cgi/darcs.cgi : \
   config.status autoconf.mk.in src/Autoconf.lhs.in tools/cgi/darcs.cgi.in
	./config.status

config.status:	configure
	@if test -x config.status; then					\
	  ./config.status --recheck;					\
	else								\
	  echo >&2 "Please run ./configure to setup the build system.";	\
	  exit 1;							\
	fi

.depend:	$(DARCS_FILES) $(UNIT_FILES)
	$(ANNOUNCE_GHC) $(GHCFLAGS) -M -optdep-f -optdep.depend $^

.depend:	autoconf.mk

ifeq (,$(findstring disttest,$(MAKECMDGOALS)))
ifeq (,$(findstring clean,$(MAKECMDGOALS)))
ifneq (config,$(MAKECMDGOALS))
include .depend
endif
endif
endif

ETAGS = hasktags -e
CTAGS = hasktags -c
# This follows what autoconf does.
# Fixme: Generate correct list of .c files and depend on that.
.PHONY: CTAGS ctags tags
tags: TAGS
TAGS: $(DARCS_FILES)
	$(ETAGS) $(DARCS_FILES) && etags -a src/*.c
ctags: CTAGS
CTAGS: $(DARCS_FILES)
	$(CTAGS) $(DARCS_FILES) && ctags -a src/*.c
	mv tags tags.unsorted
	sort tags.unsorted > tags
