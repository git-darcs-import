<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
  <head>
    <title>darcs</title>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <meta name="description" content="darcs is an advanced revision control system.">
    <meta name="keywords" content="darcs scm subversion alternative CVS free software gpl linux debian">
    <link rel="stylesheet" type="text/css" href="darcs.css">
  </head>

  <body>

    <div align="center">
      <img src="http://www.darcs.net/logos/logo.png" border="0" width="242" height="79" alt=""><br>
      <strong>Distributed. Interactive. Smart.</strong>
    </div>

    <table align="center">
      <tr>
        <td width="150" valign="top">
          <p>&nbsp;</p>
          <p>&nbsp;</p>
          <p>&nbsp;</p>
          <div style="font-size:smaller; background-color:#CCCCFF; padding: 4px; ">
            <i>"Project leaders should certainly consider its adoption." <br>
            &mdash;<a href="http://lwn.net/Articles/109719/">Linux Weekly News</a></i>
          </div>


        </td>
        <td width="15"></td>
        <td width="500">

          <p><a name="intro">Darcs is a free, open source source code management system.</a> </p>

          <h2>Distributed</h2>

          <p>Every user has access to the full command set, removing 
          boundaries between server and client or committer and non-committers.</p>

          <h2>Interactive</h2>

          <p>Darcs is easy to learn and efficient to use because it asks
          you questions in response to simple commands, giving you choices
          in your work flow. You can choose to record
          one change in a file, while ignoring another. As you update
          from upstream, you can review each patch name, even the full "diff"
          for interesting patches.  </p>

          <h2>Smart</h2>

          <p>Originally developed by physicist David Roundy, darcs is
          based on a unique algebra of patches. </p>

          <p>This smartness lets you respond to changing demands 
          in ways that would otherwise not be possible. For example, learn
          about <a href="http://wiki.darcs.net/DarcsWiki/SpontaneousBranches">spontaneous branches</a> with darcs.</p>

          <h2>Going Places</h2>
          <p>First released in 2003, Darcs is now managed by a sizable
          team of programmers (including David). Daily improvements,
          combined with tight quality control, culminate in frequent
          releases.</p>

          <hr>



          <h3>Download</h3>

          <p>
          <a href="http://wiki.darcs.net/DarcsWiki/CategoryBinaries">Binaries and source of the latest stable release</a> of darcs for various platforms including Windows, Mac OS X, FreeBSD and its siblings, Solaris, and AIX, a dozen flavors of Linux, and Cygwin.</p>

          <p>While using a binary from above is recommend, here is the latest source code for those who want it.</p>
          <ul>
            <li><a href="darcs-VERSION.tar.gz">Latest Prerelease Source</a> (VERSION).  <br/></li>
            <li><a href="darcs-RELEASE.tar.gz">Latest Stable Source</a> (RELEASE).  <br/></li>
          </ul>

          <h3>Documentation</h3>

          <ul>
            <li><a href="manual">darcs manual</a> <br></li>
          </ul>

          <h3>Community</h3>

          <ul>
            <li><strong><a href="http://wiki.darcs.net/DarcsWiki">The Darcs Wiki</a></strong> -- The best source for further information.</li>
            <li><a href="http://lists.osuosl.org/mailman/listinfo/darcs-users">users mailing list</a> </li>
            <li><a href="http://lists.osuosl.org/mailman/listinfo/darcs-devel">developers mailing list</a> </li>
            <li><a href="http://bugs.darcs.net/?user=guest;pass=guest">Bug Tracking System</a> </li>
            <li><a href="http://allmydata.org/trac/darcs-2/browser">Darcs repository browser</a></li>
          </ul>

          <h3>Darcs source repository</h3>
          <ul>
            <li>darcs get --lazy http://darcs.net (stable branch) </li>
            <li>darcs get --lazy http://darcs.net/unstable (unstable branch) </li>
          </ul>
          <p>Note, to get the Darcs source you must already have Darcs version 2 or greater (Darcs 1 will give an obscure error). If necessary, download a binary or build the source tarball to get a working Darcs 2.</p>

        </td>
        <td width="150"></td> 
    </tr></table>

  </body>
</html>
